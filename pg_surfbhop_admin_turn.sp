#include <sourcemod>

#include <influx/core>
#include <influx/stocks_strf>

#undef REQUIRE_PLUGIN
#include <influx/practise>
#include <influx/pause>

#define NEXT_PRINT          3.0
#define RESET_TIME          0.1
#define MAX_CONT_STRFS      6

Strafe_t g_iLastStrafe[INF_MAXPLAYERS];
int g_nContStrfs[INF_MAXPLAYERS];
float g_flLastStrf[INF_MAXPLAYERS];
float g_flLastPrint[INF_MAXPLAYERS];

bool g_bLate;

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Admin - Turn Block",
    description = "",
    version = INF_VERSION
};

public APLRes AskPluginLoad2( Handle hPlugin, bool late, char[] szError, int error_len )
{
    g_bLate = late;
}

public void OnPluginStart()
{ 
    
    if ( g_bLate )
    {
        for ( int i = 1; i <= MaxClients; i++ )
        {
            if ( IsClientInGame( i ) )
            {
                OnClientPutInServer( i );
            }
        }
    }
}

public void OnClientPutInServer( int client )
{
    g_iLastStrafe[client] = STRF_INVALID;
    g_nContStrfs[client] = 0;
    g_flLastStrf[client] = 0.0;
    g_flLastPrint[client] = 0.0;
}

public Action OnPlayerRunCmd( int client, int &buttons, int &impulse, float vel[3], float angles[3] )
{
    if ( !IsPlayerAlive( client ) ) return Plugin_Continue;
    
    if ( IsFakeClient( client ) ) return Plugin_Continue;
    
    static float flLastValidYaw[INF_MAXPLAYERS];
    
    bool reset = false;
    
    // Holding both buttons down doesn't actually do anything.
    // Ignore it.
    int res = buttons & (IN_LEFT|IN_RIGHT);
    
    if ( res && res != (IN_LEFT|IN_RIGHT) )
    {
        Strafe_t strf = ( res == IN_LEFT ) ? STRF_LEFT : STRF_RIGHT;
            
        if ( g_iLastStrafe[client] != STRF_INVALID && g_iLastStrafe[client] != strf )
        {
            ++g_nContStrfs[client];
        }   
            
        g_iLastStrafe[client] = strf;
        g_flLastStrf[client] = GetEngineTime();
    }
    else
    {
        flLastValidYaw[client] = angles[1];
    }
    
    
    if ( g_nContStrfs[client] > 0 )
    {
        if ( g_nContStrfs[client] >= MAX_CONT_STRFS )
        {
            reset = true;
        }
        
        
        if ( (GetEngineTime() - g_flLastStrf[client]) > RESET_TIME )
        {
            g_nContStrfs[client] = 0;
        }
    }
    
    
    if ( reset )
    {
        angles[1] = flLastValidYaw[client];
        
        if ( (GetEngineTime() - g_flLastPrint[client]) > NEXT_PRINT )
        {
            Influx_PrintToChat( _, client, "Please don't spam {MAINCLR1}+left{CHATCLR}/{MAINCLR1}+right{CHATCLR}!" );
            g_flLastPrint[client] = GetEngineTime();
        }
    }
    
    return Plugin_Continue;
}