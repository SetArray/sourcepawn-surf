#include <sourcemod>
#include <clientprefs>

#include <influx/core>
#include <influx/hud_draw>
#include <influx/options>

#include <msharedutil/misc>

#undef REQUIRE_PLUGIN
#include <influx/hud>
#include <influx/help>
#include <influx/recording>
#include <influx/strafes>
#include <influx/jumps>
#include <influx/pause>
#include <influx/practise>
#include <influx/strfsync>
#include <influx/truevel>
#include <influx/zones_stage>
#include <influx/zones_checkpoint>
#include <influx/maprankings>
#include <influx/style_tas>

float g_flJoin[INF_MAXPLAYERS];

float g_fPos[2];
int g_iClr[4];

#define HIDE_RADAR_CSGO 1<<12

// LIBRARIES
bool g_bLib_Hud;
bool g_bLib_Strafes;
bool g_bLib_Jumps;
bool g_bLib_Pause;
bool g_bLib_Practise;
bool g_bLib_Recording;
bool g_bLib_StrfSync;
bool g_bLib_Truevel;
bool g_bLib_Stage;
bool g_bLib_CP;
bool g_bLib_MapRanks;
bool g_bLib_Style_Tas;

Handle g_hCookie_ShowSidebar;
Handle g_hCookie_ShowTimer;
int g_fShowSidebar[MAXPLAYERS+1];
int g_fShowTimer[MAXPLAYERS+1];
float g_SpeedPrevious[MAXPLAYERS+1];

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - HUD Manager",
    description = "",
    version = INF_VERSION
};

public APLRes AskPluginLoad2( Handle hPlugin, bool late, char[] szError, int error_len )
{
    if ( GetEngineVersion() != Engine_CSGO )
    {
        FormatEx( szError, error_len, "Bad engine version!" );
        
        return APLRes_Failure;
    }
    
    return APLRes_Success;
}

public void OnPluginStart()
{
    // LIBRARIES
    g_bLib_Hud = LibraryExists( INFLUX_LIB_HUD );
    g_bLib_Strafes = LibraryExists( INFLUX_LIB_STRAFES );
    g_bLib_Jumps = LibraryExists( INFLUX_LIB_JUMPS );
    g_bLib_Pause = LibraryExists( INFLUX_LIB_PAUSE );
    g_bLib_Practise = LibraryExists( INFLUX_LIB_PRACTISE );
    g_bLib_Recording = LibraryExists( INFLUX_LIB_RECORDING );
    g_bLib_StrfSync = LibraryExists( INFLUX_LIB_STRFSYNC );
    g_bLib_Truevel = LibraryExists( INFLUX_LIB_TRUEVEL );
    g_bLib_Stage = LibraryExists( INFLUX_LIB_ZONES_STAGE );
    g_bLib_CP = LibraryExists( INFLUX_LIB_ZONES_CP );
    g_bLib_MapRanks = LibraryExists( INFLUX_LIB_MAPRANKS );
    g_bLib_Style_Tas = LibraryExists( INFLUX_LIB_STYLE_TAS );

    // HIDE RADAR HOOK
    HookEvent("player_spawn", Influx_HideDisplay);

    if ( (g_hCookie_ShowTimer = RegClientCookie( "influx_showtimer", INF_NAME..." Show Timer", CookieAccess_Protected )) == null )
    {
        SetFailState( INF_CON_PRE..."Couldn't register showtimer cookie!" );
    } 
    if ( (g_hCookie_ShowSidebar = RegClientCookie( "influx_showsidebar", INF_NAME..." Show Sidebar", CookieAccess_Protected )) == null )
    {
        SetFailState( INF_CON_PRE..."Couldn't register showsidebar cookie!" );
    }       

    RegConsoleCmd( "sm_showtimer", Cmd_ShowTimer );  
    RegConsoleCmd( "sm_showsidebar", Cmd_ShowSidebar );  
}

public void Influx_OnRequestOptionsCmds()
{
    Influx_AddOptionsCommand( "showtimer", "Toggle your HUD Timer ON/OFF." );
    Influx_AddOptionsCommand( "showsidebar", "Toggle your HUD Sidebar ON/OFF." );    
}

public void OnClientCookiesCached(int client)
{
    if(AreClientCookiesCached(client))
    {
        char szCookie[12];
        GetClientCookie(client, g_hCookie_ShowSidebar, szCookie, sizeof(szCookie));

        if(szCookie[0] != '\0')
        {
            g_fShowSidebar[client] = StringToInt(szCookie);
        }
        else
        {
            g_fShowSidebar[client] = 1;
        }

        GetClientCookie(client, g_hCookie_ShowTimer, szCookie, sizeof(szCookie));

        if(szCookie[0] != '\0')
        {
            g_fShowTimer[client] = StringToInt(szCookie);
        }
        else
        {
            g_fShowTimer[client] = 1;
        }        
    }
    else
    {
        g_fShowSidebar[client] = 1;
        g_fShowTimer[client] = 1;
    }    
}

public Action Cmd_ShowSidebar(int client, int args)
{
    if (!client) return Plugin_Handled;

    if(g_fShowSidebar[client] != 1)
    {
        g_fShowSidebar[client] = 1;
    }
    else
    {
        g_fShowSidebar[client] = 0;
    }

    return Plugin_Handled;
}

public Action Cmd_ShowTimer(int client, int args)
{
    if (!client) return Plugin_Handled;

    if(g_fShowTimer[client] != 1)
    {
        g_fShowTimer[client] = 1;
    }
    else
    {
        g_fShowTimer[client] = 0;
    }

    return Plugin_Handled;
}

public void OnClientDisconnect(int client)
{
    if(IsFakeClient(client)) return;

    char szCookie[12];
    IntToString(g_fShowTimer[client], szCookie, sizeof(szCookie));
    SetClientCookie(client, g_hCookie_ShowTimer, szCookie);
    IntToString(g_fShowSidebar[client], szCookie, sizeof(szCookie));
    SetClientCookie(client, g_hCookie_ShowSidebar, szCookie);        
}

public void OnLibraryAdded( const char[] lib )
{
    if ( StrEqual( lib, INFLUX_LIB_HUD ) ) g_bLib_Hud = true;
    if ( StrEqual( lib, INFLUX_LIB_STRAFES ) ) g_bLib_Strafes = true;
    if ( StrEqual( lib, INFLUX_LIB_JUMPS ) ) g_bLib_Jumps = true;
    if ( StrEqual( lib, INFLUX_LIB_PAUSE ) ) g_bLib_Pause = true;
    if ( StrEqual( lib, INFLUX_LIB_RECORDING ) ) g_bLib_Recording = true;
    if ( StrEqual( lib, INFLUX_LIB_STRFSYNC ) ) g_bLib_StrfSync = true;
    if ( StrEqual( lib, INFLUX_LIB_TRUEVEL ) ) g_bLib_Truevel = true;
    if ( StrEqual( lib, INFLUX_LIB_ZONES_CP ) ) g_bLib_CP = true;
    if ( StrEqual( lib, INFLUX_LIB_MAPRANKS ) ) g_bLib_MapRanks = true;
    if ( StrEqual( lib, INFLUX_LIB_STYLE_TAS ) ) g_bLib_Style_Tas = true;
}

public void OnLibraryRemoved( const char[] lib )
{
    if ( StrEqual( lib, INFLUX_LIB_HUD ) ) g_bLib_Hud = false;
    if ( StrEqual( lib, INFLUX_LIB_STRAFES ) ) g_bLib_Strafes = false;
    if ( StrEqual( lib, INFLUX_LIB_JUMPS ) ) g_bLib_Jumps = false;
    if ( StrEqual( lib, INFLUX_LIB_PAUSE ) ) g_bLib_Pause = false;
    if ( StrEqual( lib, INFLUX_LIB_RECORDING ) ) g_bLib_Recording = false;
    if ( StrEqual( lib, INFLUX_LIB_STRFSYNC ) ) g_bLib_StrfSync = false;
    if ( StrEqual( lib, INFLUX_LIB_TRUEVEL ) ) g_bLib_Truevel = false;
    if ( StrEqual( lib, INFLUX_LIB_ZONES_CP ) ) g_bLib_CP = false;
    if ( StrEqual( lib, INFLUX_LIB_MAPRANKS ) ) g_bLib_MapRanks = false;
    if ( StrEqual( lib, INFLUX_LIB_STYLE_TAS ) ) g_bLib_Style_Tas = false;
}

public void OnClientPutInServer( int client )
{
    g_flJoin[client] = GetEngineTime();
}

public Action Influx_HideDisplay(Handle:event, const String:name[], bool:uselessShit)
{
    new client = GetClientOfUserId(GetEventInt(event, "userid"));
    CreateTimer(0.0, Influx_HideDisplay_Post, client);
}

public Action Influx_HideDisplay_Post(Handle:timer, any:client)
{
    SetEntProp(client, Prop_Send, "m_iHideHUD", GetEntProp(client, Prop_Send, "m_iHideHUD") | HIDE_RADAR_CSGO);
}

public Action Influx_OnDrawHUD( int client, int target, HudType_t hudtype )
{
    static char szMsg[256];
    szMsg[0] = 0;
    
    static char szTemp[64];
    static char szTemp2[64];
    static char szTemp3[64];
    
    decl String:szSecFormat[12];
    
    RunState_t state = Influx_GetClientState( target );
    
    decl curlinelen;
    
    bool bIsReplayBot = ( IsFakeClient( target ) && g_bLib_Recording && Influx_GetReplayBot() == target );    
    
    if ( hudtype == HUDTYPE_TIMER )
    {
        Influx_GetSecondsFormat_Timer( szSecFormat, sizeof( szSecFormat ) );
        curlinelen = 0;

        // FIRST LINE OF TEXT - TIMER
        if( !bIsReplayBot )
        {
        	if(state == STATE_START)
        	{
	            Influx_GetRunName( Influx_GetClientRunId( target ), szTemp2, sizeof( szTemp2 ) );
	            curlinelen = Format( szTemp2, sizeof( szTemp2 ), "Stopped", szTemp2 );
	            
	            Format( szMsg, sizeof( szMsg ), "<font color='#E10000'>%s</font>", szTemp2 );        		
        	} 
        	else if ( state >= STATE_RUNNING )
        	{
				float time = INVALID_RUN_TIME;
                float cptime = INVALID_RUN_TIME;
	            
	            decl String:pre[2];
	            pre[0] = 0;
	            pre[1] = 0;
	            
	            decl String:szForm[32];
	            strcopy( szForm, sizeof( szForm ), "%05.2f" );
	            
	            decl String:szTimeName[32];
	            strcopy( szTimeName, sizeof( szTimeName ), "Time: " );
	            
	            decl String:szColor[32];
	            szColor[0] = 0;
	            
	            if (g_bLib_CP &&  (GetEngineTime() - Influx_GetClientLastCPTouch( target )) < 2.0)
	            {
	                cptime = Influx_GetClientLastCPSRTime( target );
	                
	                // Fallback to best time if no SR time is found.
	                if ( cptime == INVALID_RUN_TIME ) cptime = Influx_GetClientLastCPBestTime( target );
	            }
	            
	            
	            //if ( IsFakeClient( target ) )
	            if ( state == STATE_FINISHED )
	            {
	                time = Influx_GetClientFinishedTime( target );
	            }
	            else if ( g_bLib_Pause && Influx_IsClientPaused( target ) )
	            {
	                time = Influx_GetClientPausedTime( target );
	            }
	            else if ( cptime != INVALID_RUN_TIME )
	            {
	                float lastcptime = Influx_GetClientLastCPTime( target );
	                
	                
	                
	                time = Inf_GetTimeDif( lastcptime, cptime, view_as<int>( pre[0] ) );
	                strcopy( szForm, sizeof( szForm ), szSecFormat );
	                
	                strcopy( szColor, sizeof( szColor ), " color='#00cc00'" );
	                
	                strcopy( szTimeName, sizeof( szTimeName ), "CP: " );
	            }
	            else if ( g_bLib_Style_Tas && Influx_GetClientStyle( target ) == STYLE_TAS )
	            {
	                time = Influx_GetClientTASTime( target );
	                strcopy( szForm, sizeof( szForm ), szSecFormat );
	            }
	            else
	            {
	                time = Influx_GetClientTime( target );
	                strcopy( szForm, sizeof( szForm ), szSecFormat );
	                
	                strcopy( szColor, sizeof( szColor ), " color='#00cc00'" );
	            }

	            if(state == STATE_RUNNING)
	            {
	            	float time_pb = Influx_GetClientCurrentPB( target );
	            	float time_wr = Influx_GetClientCurrentBestTime( target );
	            	if ( time_pb > INVALID_RUN_TIME )
	            	{
	            		if(time > time_pb)
	            		{
	            			strcopy( szColor, sizeof( szColor ), " color='#cc0000'" );
	            		}
	            		else if(time_wr > INVALID_RUN_TIME)
	            		{
	            			if(time > time_wr)
	            			{
	            				strcopy( szColor, sizeof( szColor ), " color='#cccc00'" );
	            			}
	            		}
	            	}
	            }
	            
	            if ( time != INVALID_RUN_TIME )
	            {
	                curlinelen += strlen( pre );
	                
	                
	                curlinelen += strlen( szTimeName );
	                
	                Inf_FormatSeconds( time, szTemp, sizeof( szTemp ), szForm );
	                
	                curlinelen += strlen( szTemp );
	                
	                Format( szMsg, sizeof( szMsg ), "%s<font%s>%s%s</font>",
	                    szTimeName,
	                    szColor,
	                    ( pre[0] != 0 ) ? pre : "",
	                    szTemp );
	            }
        	}
        } 
        else 
        {
            Inf_FormatSeconds( Influx_GetReplayTime(), szTemp, sizeof( szTemp ), szSecFormat );
            curlinelen = FormatEx( szTemp2, sizeof( szTemp2 ), "Time: %s", szTemp );
            Format( szMsg, sizeof( szMsg ), "%s", szTemp2 );
        }

        // FIRST LINE OF TEXT - SPEED
        GetTabs( curlinelen, szTemp, sizeof( szTemp ) );

        float lSpeed = GetSpeed(target); 
        char szSpeedColor[64];
        if(lSpeed > g_SpeedPrevious[target])
        {
        	strcopy(szSpeedColor, 64, "<font color='#4286f4'>");
        }
        else if(lSpeed < g_SpeedPrevious[target])
        {
        	strcopy(szSpeedColor, 64, "<font color='#cc0000'>");
        }
        else
        {
        	strcopy(szSpeedColor, 64, "<font color='#FFFFFF'>");
        }
        g_SpeedPrevious[target] = lSpeed;
        
        Format( szMsg, sizeof( szMsg ), "%s%sSpeed: %s%03.0f</font>",
            szMsg,
            szTemp,
            szSpeedColor,
            g_SpeedPrevious[target] );

        // FIRST LINE OF TEXT - FINALIZE
        AddAndGotoLine( szMsg, szMsg, sizeof( szMsg ), 2 );
    	curlinelen = 0;

    	// SECOND LINE OF TEXT - PERSONAL GAME (PB AND SR)
    	if( !bIsReplayBot && client == target)
    	{
            if ( /*!(hideflags & HIDEFLAG_PB_TIME) && */Influx_IsClientCached( target ) )
            {
                float time = Influx_GetClientCurrentPB( target );
                if ( time > INVALID_RUN_TIME )
                {
                    Inf_FormatSeconds( time, szTemp, sizeof( szTemp ), szSecFormat );
                    curlinelen = FormatEx( szTemp3, sizeof( szTemp3 ), "PB: %s", szTemp );
                }
                else
                {
                    curlinelen = FormatEx( szTemp3, sizeof( szTemp3 ), "PB: N/A" );
                }
            } 

            float time = Influx_GetClientCurrentBestTime( target );
                
            if ( time > INVALID_RUN_TIME )
            {
                Inf_FormatSeconds( time, szTemp2, sizeof( szTemp2 ), szSecFormat );                   
                FormatEx( szTemp, sizeof( szTemp ), "SR: %s", szTemp2 );
            }
            else
            {
                strcopy( szTemp, sizeof( szTemp ), "SR: N/A" );
            }
                
            GetTabs( curlinelen, szTemp2, sizeof( szTemp2 ) );
                
            Format( szMsg, sizeof( szMsg ), "%s%s%s%s",
                szMsg,
                szTemp3,
                szTemp2,
                szTemp );

            AddAndGotoLine( szMsg, szMsg, sizeof( szMsg ), 3 );
            curlinelen = 0;   		
    	}
    	// SECOND LINE OF TEXT - PLAYER SPECTATE (JUMPS AND STRAFE/SYNC)
    	if(!bIsReplayBot && client != target) 
    	{
	        /**if ( g_bLib_Jumps && state >= STATE_RUNNING && Influx_IsCountingJumps( target ) )
	        {
	        	curlinelen = FormatEx( szTemp3, sizeof( szTemp3 ), "Jumps: %i", Influx_GetClientJumpCount( target ) );
	        }   
	        else
	        {
	        	curlinelen = FormatEx( szTemp3, sizeof( szTemp3 ), "Jumps: N/A" );
	        }*/
	        
	        if ( Influx_ShouldDisplayStages( target ) )
	        {
	            int stages = Influx_GetClientStageCount( target );
	            
	            if ( stages < 2 )
	            {
	                strcopy( szTemp, sizeof( szTemp ), "Linear" );
	            }
	            else
	            {
	                FormatEx( szTemp, sizeof( szTemp ), "%i/%i", Influx_GetClientStage( target ), stages );
	            }
	            
	            curlinelen = FormatEx( szTemp3, sizeof( szTemp3 ), "Stage: %s", szTemp );
	        }
	        else
	        {
	        	strcopy(szTemp, sizeof(szTemp), "N/A");
	        	curlinelen = FormatEx( szTemp3, sizeof(szTemp3), "Stage: %s", szTemp);
	        }	        

	        GetTabs( curlinelen, szTemp, sizeof( szTemp ) );	
			
	        if ( state >= STATE_RUNNING && g_bLib_Strafes && g_bLib_StrfSync)
        	{
        		FormatEx(szTemp2, sizeof(szTemp2), "Strafes: %i (%.1f﹪)", 
        			Influx_GetClientStrafeCount( target ),
        			Influx_GetClientStrafeSync( target )); 
        	}
        	else
        	{
        		strcopy(szTemp2, sizeof(szTemp2), "Strafes: N/A (N/A﹪)");
        	}

            Format(szMsg, sizeof(szMsg), "%s%s%s%s",
                szMsg,
                szTemp3,
                szTemp,
                szTemp2);

            AddAndGotoLine( szMsg, szMsg, sizeof( szMsg ), 3 );
            curlinelen = 0;
    	}
    	// SECOND LINE OF TEXT - BOT SPECTATE (STYLE AND MODE)
    	if(bIsReplayBot)
    	{
	        int targetmode = MODE_INVALID;
	        int targetstyle = STYLE_INVALID;
	        
	        if ( bIsReplayBot )
	        {
	            targetmode = Influx_GetReplayMode();
	            targetstyle = Influx_GetReplayStyle();
	        }

        	Influx_GetModeShortName( targetmode, szTemp3, sizeof( szTemp3 ), true );
        	Influx_GetStyleShortName( targetstyle, szTemp2, sizeof( szTemp2 ), true );

        	if(targetstyle == 0)
        	{
        		strcopy(szTemp2, sizeof(szTemp2), "NM");
        	}

        	Format(szMsg, sizeof(szMsg), "%sStyle: <font color='#4286f4'>%s-%s</font>",
                szMsg,
                szTemp3,
                szTemp2);

            AddAndGotoLine( szMsg, szMsg, sizeof( szMsg ), 3 );
            curlinelen = 0;
    	}
    	// THIRD LINE OF TEXT - PERSONAL GAME (RANK AND STYLE)
    	if(!bIsReplayBot && client == target && state == STATE_START)
    	{
    		if(!IsFakeClient(target))
    		{
	            if ( g_bLib_MapRanks )
	            {
	                int rank = Influx_GetClientCurrentMapRank( target );
	                int numrecs = Influx_GetClientCurrentMapRankCount( target );
	                
	                if ( numrecs > 0 )
	                {
	                    if ( rank > 0 )
	                    {
	                        curlinelen = FormatEx( szTemp, sizeof( szTemp ), "Rank: %i/%i", rank, numrecs );
	                    }
	                    else
	                    {
	                        curlinelen = FormatEx( szTemp, sizeof( szTemp ), "Rank: -/%i", numrecs );
	                    }
	                }
	                else
	                {
	                    curlinelen = FormatEx( szTemp, sizeof( szTemp ), "Rank: -/-" );
	                }
	                
	                Format( szMsg, sizeof( szMsg ), "%s%s", szMsg, szTemp );
	            }   
	            
	            GetTabs( curlinelen, szTemp, sizeof( szTemp ) );	

	            int targetmode = MODE_INVALID;
                int targetstyle = STYLE_INVALID;		
                targetmode = Influx_GetClientMode( target );
                targetstyle = Influx_GetClientStyle( target );	

                Influx_GetModeShortName( targetmode, szTemp3, sizeof( szTemp3 ), true );
                Influx_GetStyleShortName( targetstyle, szTemp2, sizeof( szTemp2 ), true );            	       	
        		if(targetstyle == 0)
        		{
        			strcopy(szTemp2, sizeof(szTemp2), "NM");
        		}
        		Format(szTemp2, sizeof(szMsg), "Style: <font color='#4286f4'>%s-%s</font>",
        			szTemp3,
        			szTemp2);  

        		Format(szMsg, sizeof(szMsg), "%s%s%s",
        			szMsg,
        			szTemp,
        			szTemp2);        		  		
    		}
    	}	
    	// THIRD LINE OF TEXT - PERSONAL GAME (JUMPS AND STRAFE/SYNC)
      	if(!bIsReplayBot && client == target && state != STATE_START)
      	{
	        /**if ( g_bLib_Jumps && state >= STATE_RUNNING && Influx_IsCountingJumps( target ) )
	        {
	        	curlinelen = FormatEx( szTemp3, sizeof( szTemp3 ), "Jumps: %i", Influx_GetClientJumpCount( target ) );
	        }   
	        else
	        {
	        	curlinelen = FormatEx( szTemp3, sizeof( szTemp3 ), "Jumps: N/A" );
	        }*/
	        
	        if ( Influx_ShouldDisplayStages( target ) )
	        {
	            int stages = Influx_GetClientStageCount( target );
	            
	            if ( stages < 2 )
	            {
	                strcopy( szTemp, sizeof( szTemp ), "Linear" );
	            }
	            else
	            {
	                FormatEx( szTemp, sizeof( szTemp ), "%i/%i", Influx_GetClientStage( target ), stages );
	            }
	            
	            curlinelen = FormatEx( szTemp3, sizeof( szTemp3 ), "Stage: %s", szTemp );
	        }
	        else
	        {
	        	strcopy(szTemp, sizeof(szTemp), "N/A");
	        	curlinelen = FormatEx( szTemp3, sizeof(szTemp3), "Stage: %s", szTemp);
	        }
	        
            GetTabs( curlinelen, szTemp, sizeof( szTemp ) );	
			
	        if ( state >= STATE_RUNNING && g_bLib_Strafes && g_bLib_StrfSync)
        	{
        		FormatEx(szTemp2, sizeof(szTemp2), "Strafes: %i (%.1f﹪)", 
        			Influx_GetClientStrafeCount( target ),
        			Influx_GetClientStrafeSync( target )); 
        	}
        	else
        	{
        		strcopy(szTemp2, sizeof(szTemp2), "Strafes: N/A (100.0﹪)");
        	}

        	Format(szMsg, sizeof(szMsg), "%s%s%s%s",
        		szMsg,
        		szTemp3,
        		szTemp,
        		szTemp2);  		
      	}
      	// THRID LINE OF TEXT - SPECTATING HUMAN
      	if(!bIsReplayBot && client != target)
      	{
      		int targetmode = MODE_INVALID;
	        int targetstyle = STYLE_INVALID;

            targetmode = Influx_GetClientMode( target );
            targetstyle = Influx_GetClientStyle( target );

            Influx_GetModeShortName( targetmode, szTemp3, sizeof( szTemp3 ), true );
        	Influx_GetStyleShortName( targetstyle, szTemp2, sizeof( szTemp2 ), true );

        	if(targetstyle == 0)
        	{
        		strcopy(szTemp2, sizeof(szTemp2), "NM");
        	}

        	Format(szMsg, sizeof(szMsg), "%sStyle: <font color='#4286f4'>%s-%s</font>",
        		szMsg,
        		szTemp3,
        		szTemp2);
      	}

        if ( szMsg[0] != 0 && g_fShowTimer[client] == 1)
        {
            Format( szMsg, sizeof( szMsg ), "<font size='22' face='monospace'>%s</font>", szMsg );
            
            PrintHintText( client, szMsg );
        }
    }
    else if ( hudtype == HUDTYPE_HUDMSG )
    {
        Influx_GetSecondsFormat_Sidebar( szSecFormat, sizeof( szSecFormat ) );
        
        // Disable for bots.
        if ( IsFakeClient( target ) )
        {
            return Plugin_Stop;
        }    

        float time = Influx_GetClientCurrentBestTime( target );
        if( time > INVALID_RUN_TIME )
        {
            Inf_FormatSeconds( time, szTemp3, sizeof( szTemp3 ), szSecFormat );
            Influx_GetClientCurrentBestName( target, szTemp2, sizeof( szTemp2 ) ); 

            FormatEx(szTemp, sizeof(szTemp), " SERVER RECORD: %s (%s)", szTemp3, szTemp2);           
        }
        else
        {
            strcopy(szTemp, sizeof(szTemp), " SERVER RECORD: N/A");
        }

        FormatEx(szMsg, sizeof(szMsg), "%s", szTemp);
        ADD_SEPARATOR( szMsg, "\n " );

        time = Influx_GetClientCurrentPB( target );
        if( time > INVALID_RUN_TIME )
        {
            Inf_FormatSeconds( time, szTemp3, sizeof( szTemp3 ), szSecFormat );

            FormatEx(szTemp, sizeof(szTemp), "PERSONAL BEST: %s", szTemp3);           
        }
        else
        {
            strcopy(szTemp, sizeof(szTemp), "PERSONAL BEST: N/A");
        }    

        Format(szMsg, sizeof(szMsg), "%s%s",
            szMsg,
            //NEWLINE_CHECK(szMsg),
            szTemp);

        ADD_SEPARATOR(szMsg, "\n ");

        if ( Influx_ShouldDisplayStages( target ) )
        {
            int stages = Influx_GetClientStageCount( target );
            
            if ( stages < 2 )
            {
                strcopy( szTemp2, sizeof( szTemp2 ), "LINEAR" );
            }
            else
            {
                FormatEx( szTemp2, sizeof( szTemp2 ), "%i/%i", Influx_GetClientStage( target ), stages );
            }
            
            FormatEx( szTemp, sizeof( szTemp ), "STAGE: %s", szTemp2 );
        }

        Format(szMsg, sizeof(szMsg), "%s%s",
            szMsg,
            //NEWLINE_CHECK(szMsg),
            szTemp);
        
        if(g_fShowSidebar[client] == 0)
        {
            DisplayHudMsg( client, szMsg );
        }
    }
    
    return Plugin_Stop;
}

// Check if they want truevel.
stock float GetSpeed( int client )
{
    return ( g_bLib_Truevel && Influx_IsClientUsingTruevel( client ) ) ? GetEntityTrueSpeed( client ) : GetEntitySpeed( client );
}

public int Hndlr_Panel_Empty( Menu menu, MenuAction action, int client, int param2 ) {}


stock bool AddAndGotoLine( const char[] sz, char[] out, int len, int wantedline )
{
    if ( wantedline < 2 ) return false;
    
    
    int numlines = 1;
    
    int start = 0;
    decl pos;
    while ( (pos = FindCharInString( sz[start], '\n' )) != -1 )
    {
        ++numlines;
        
        start += pos + 1;
    }
    
    if ( numlines >= wantedline ) return false;
    
    
    while ( numlines < wantedline )
    {
        int lastpos = strlen( out ) - 1;
        
        if ( lastpos < 0 ) lastpos = 0;
        
        Format( out, len, "%s%s\n", out, (lastpos == 0 || out[lastpos] == '\n') ? " " : "" );
        
        ++numlines;
    }
    
    return true; 
}

stock void DisplayHudMsg( int client, const char[] msg )
{
    int clients[1];
    clients[0] = client;
    
    g_fPos[0] = 0.02;
    g_fPos[1] = 0.02;
    g_iClr[0] = 255;
    g_iClr[1] = 255;
    g_iClr[2] = 255;
    g_iClr[3] = 255;
    SendHudMsg( clients, 1, msg, 2, g_fPos, g_iClr, g_iClr, 0, 0.0, 0.0, 1.0, 0.0 );
}

stock void SendHudMsg(  int[] clients,
                        int nClients,
                        const char[] text,
                        int channel,
                        const float pos[2],
                        const int clr1[4],
                        const int clr2[4],
                        int effect,
                        float fade_in,
                        float fade_out,
                        float hold_time,
                        float fx_time )
{
    static UserMsg UserMsg_HudMsg = INVALID_MESSAGE_ID;
    
    if ( UserMsg_HudMsg == INVALID_MESSAGE_ID )
    {
        if ( (UserMsg_HudMsg = GetUserMessageId( "HudMsg" )) == INVALID_MESSAGE_ID )
        {
            SetFailState( INF_CON_PRE..."Couldn't find usermessage id for HudMsg!" );
        }
    }
    
    
    Handle hMsg = StartMessageEx( UserMsg_HudMsg, clients, nClients, USERMSG_BLOCKHOOKS );
    
    if ( hMsg != null )
    {
        if ( GetUserMessageType() == UM_Protobuf )
        {
            PbSetInt( hMsg, "channel", channel );
            PbSetVector2D( hMsg, "pos", pos );
            PbSetColor( hMsg, "clr1", clr1 );
            PbSetColor( hMsg, "clr2", clr2 );
            PbSetInt( hMsg, "effect", effect );
            PbSetFloat( hMsg, "fade_in_time", fade_in );
            PbSetFloat( hMsg, "fade_out_time", fade_out );
            PbSetFloat( hMsg, "hold_time", hold_time );
            PbSetFloat( hMsg, "fx_time", fx_time );
            PbSetString( hMsg, "text", text );
        }
        else
        {
            PrintToServer( "This shouldn't happen!" );
        }
        
        EndMessage();
    }
}

stock void GetTabs( int linelen, char[] out, int len, int numtabs = 3 )
{
    out[0] = 0;
    
    int num = numtabs - linelen / 6;

    for ( int i = 0; i < num; i++ )
    {
        Format( out, len, "%s\t", out );
    }
}

stock void AddPadding( char[] out, int len, int padding )
{
    int l = strlen( out );
    
    if ( l >= padding ) return;
    
    if ( padding >= len ) return;
    
    
    for ( int i = l; i < padding; i++ )
    {
        out[i] = ' ';
    }
    
    out[padding] = 0;
}