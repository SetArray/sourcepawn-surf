#include <sourcemod>
#include <cstrike>

#include <influx/core>

#undef REQUIRE_PLUGIN

#define PG_KILLBLOCK_HANDLER_VAL 2

// Altered for PG BHOP/SURF Release CORE 1.1.0 - PG 1.0.0
// Date of Change: September 26, 2017

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Disable Suicide",
    description = "",
    version = INF_VERSION
};

public void OnPluginStart()
{
    
    // LISTENERS
    AddCommandListener( Lstnr_Kill, "kill" );
    AddCommandListener( Lstnr_Kill, "explode" );
    
}

public Action Lstnr_Kill( int client, const char[] command, int argc )
{
    if ( !client ) return Plugin_Continue;
        
    switch ( PG_KILLBLOCK_HANDLER_VAL )
    {
        case 0 : return Plugin_Continue;
        case 1 : ChangeClientTeam( client, CS_TEAM_SPECTATOR );
        case 2 : FakeClientCommand( client, "sm_restart" );
    }
    
    return Plugin_Handled;
}