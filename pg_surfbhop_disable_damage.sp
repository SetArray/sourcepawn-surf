#include <sourcemod>
#include <cstrike>
#include <sdkhooks>

#include <influx/core>

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Disable Damage",
    description = "",
    version = INF_VERSION
};

public void OnPluginStart()
{
	HookEvent("player_spawn", OnPlayerSpawn);
}

public OnClientPutInServer(client)
{
    SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamageOverride)
}

public Action OnTakeDamageOverride( int victim, int &attacker, int &inflictor, float &damage, int &damagetype )
{
    damage = 0.0;
    return Plugin_Changed;
}

public OnPlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	SetEntProp(client, Prop_Data, "m_takedamage", 0, 1);
}
