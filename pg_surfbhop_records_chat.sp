#include <sourcemod>

#include <influx/core>

#undef REQUIRE_PLUGIN
#include <influx/hud>

bool g_bLib_Hud;

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Record Chat",
    description = "",
    version = INF_VERSION
};

public void OnPluginStart()
{    
    g_bLib_Hud = LibraryExists( INFLUX_LIB_HUD );
}

public void OnLibraryAdded( const char[] lib )
{
    if ( StrEqual( lib, INFLUX_LIB_HUD ) ) g_bLib_Hud = true;
}

public void OnLibraryRemoved( const char[] lib )
{
    if ( StrEqual( lib, INFLUX_LIB_HUD ) ) g_bLib_Hud = false;
}

public void Influx_OnRequestResultFlags()
{
    Influx_AddResultFlag( "Don't print record chat message", RES_CHAT_DONTPRINT );
}

stock bool ShouldPrint( float time, int flags )
{
    // We don't want to print for this run.
    if ( flags & RES_CHAT_DONTPRINT ) return false;
    
    
    // Let them see best records always!
    if ( flags & (RES_TIME_ISBEST | RES_TIME_FIRSTREC) ) return true;
    
    
    // Must be more than this.
    return ( time > 7.5 );
}

stock bool CanPrintToClient( int client, int finisher, int flags )
{
    int hideflags = Influx_GetClientHideFlags( client );
    
    // Allow my own sounds.
    if ( client == finisher )
    {
        return ( hideflags & HIDEFLAG_CHAT_PERSONAL ) ? false : true;
    }
    
    // Allow best sounds.
    if ( flags & (RES_TIME_ISBEST | RES_TIME_FIRSTREC) ) 
    {
        return ( hideflags & HIDEFLAG_CHAT_BEST ) ? false : true;
    }
    
    return ( hideflags & HIDEFLAG_CHAT_NORMAL ) ? false : true;
}

public void Influx_OnTimerFinishPost( int client, int runid, int mode, int style, float time, float prev_pb, float prev_best, int flags )
{
    if ( !ShouldPrint( time, flags ) ) return;
    
    
    int nClients = 0;
    int[] clients = new int[MaxClients];
    
    
    for ( int i = 1; i <= MaxClients; i++ )
    {
        if ( IsClientInGame( i ) && !IsFakeClient( i ) )
        {
            if ( g_bLib_Hud )
            {
                if ( !CanPrintToClient( i, client, flags ) ) continue;
            }
            
            clients[nClients++] = i;
        }
    }
    
    if ( !nClients ) return;
        
    // Format our second formatting string.
    decl String:szFormSec[10];
    Inf_DecimalFormat( 2, szFormSec, sizeof( szFormSec ) );
    
    
    decl String:szName[MAX_NAME_LENGTH];
    decl String:szForm[10];
    decl String:szRun[MAX_RUN_NAME];
    decl String:szRec[64];
    
    if ( prev_pb != INVALID_RUN_TIME )
    {
        int c;
        
        Inf_FormatSeconds( Inf_GetTimeDif( time, prev_pb, c ), szForm, sizeof( szForm ), szFormSec );
        
        FormatEx( szRec, sizeof( szRec ), " {CHATCLR}({%s}%c%s{CHATCLR})",
            (time < prev_pb) ? "GREEN" : "LIGHTRED", // he did an good?
            c,
            szForm );
    }
    else
    {
        szRec[0] = '\0';
    }
    
    Inf_FormatSeconds( time, szForm, sizeof( szForm ), szFormSec );    
    Influx_GetRunName( runid, szRun, sizeof( szRun ) );
    
    GetClientName( client, szName, sizeof( szName ) );
    Influx_RemoveChatColors( szName, sizeof( szName ) );

    char szStyle[32];
    Influx_GetStyleName( Influx_GetClientStyle( client ), szStyle, sizeof( szStyle ) );
    
    Influx_PrintToChatEx( _, client, clients, nClients, "{MAINCLR1}%s{CHATCLR} finished {MAINCLR1}%s (%s){CHATCLR} in %s %s",
        szName,
        szRun,
        szStyle,
        szForm,
        szRec);
}