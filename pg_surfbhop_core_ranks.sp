#include <sourcemod>
#include <float>
#include <clients>
#include <string>
#include <cstrike>

#include <influx/core>
#include <influx/simpleranks>

#include <msharedutil/misc>

#undef REQUIRE_PLUGIN
#include <influx/help>

#define INF_PRIVCOM_CUSTOMRANK          "sm_inf_customrank"
#define INF_PRIVCOM_MAPREWARD           "sm_inf_setmapreward"
#define INF_TABLE_SIMPLERANKS           "inf_simpleranks"
#define INF_TABLE_SIMPLERANKS_HISTORY   "inf_simpleranks_history"
#define INF_TABLE_SIMPLERANKS_MAPS      "inf_simpleranks_maps"
#define RANK_FILE_NAME                  "pg_surfbhop_ranks.cfg"
#define RANK_MODEPOINTFILE_NAME         "pg_surfbhop_mpoints.cfg"
#define RANK_STYLEPOINTFILE_NAME        "pg_surfbhop_spoints.cfg"
#define RANK_CUSTOMRANKS_COACH          "pg_surfbhop_coachlist.cfg"
#define RANK_CUSTOMRANKS_TESTER         "pg_surfbhop_testerlist.cfg"
#define MAX_RANK_SIZE                   128
#define MAX_RANK_SIZE_CELL              ( MAX_RANK_SIZE / 4 )

enum
{
    RANK_NAME[MAX_RANK_SIZE_CELL] = 0,
    RANK_POINTS,
    RANK_UNLOCK,
    RANK_SIZE
};

enum
{
    REWARD_RUN_ID = 0,
    REWARD_POINTS,
    REWARD_SIZE
};

#define MAX_P_NAME_ID       32
#define MAX_P_NAME_ID_CELL  ( MAX_P_NAME_ID / 4 )

enum
{
    P_NAME_ID[MAX_P_NAME_ID_CELL] = 0,
    P_ID,
    P_VAL,
    P_SIZE
}

ArrayList g_hModePoints;
ArrayList g_hStylePoints;

int g_nBestPointsStyle[11];
int g_nBestPointsUser[11];
char g_nBestPointsUserName[11][64];

int g_nPoints[INF_MAXPLAYERS];
int g_nPoints_NORMAL[INF_MAXPLAYERS];
int g_nPoints_SW[INF_MAXPLAYERS];
int g_nPoints_W[INF_MAXPLAYERS];
int g_nPoints_AD[INF_MAXPLAYERS];
int g_nPoints_HSW[INF_MAXPLAYERS];
int g_nPoints_RHSW[INF_MAXPLAYERS];
int g_nPoints_LOWGRAV[INF_MAXPLAYERS];
int g_nPoints_BWD[INF_MAXPLAYERS];
int g_nPoints_PARKOUR[INF_MAXPLAYERS];
int g_nPoints_TAS[INF_MAXPLAYERS];
int g_nPoints_PRESPEED[INF_MAXPLAYERS];

char g_szCurRank[INF_MAXPLAYERS][MAX_RANK_SIZE];
char g_szDonRank[INF_MAXPLAYERS][MAX_RANK_SIZE];
int g_iCurRank[INF_MAXPLAYERS];
bool g_bChose[INF_MAXPLAYERS];

ArrayList g_hMapRewards;
//int g_nMapReward;
Handle scoreboardPlayerRankTimer[MAXPLAYERS+1]
int g_lClientStyleID[INF_MAXPLAYERS+1];

ArrayList g_hRanks;
ArrayList g_hTesters;
ArrayList g_hCoaches;

bool g_bLate;

#include "pg_surfbhop/ranks/cmds.sp"
#include "pg_surfbhop/ranks/db.sp"
#include "pg_surfbhop/ranks/file.sp"

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Core Ranks",
    description = "",
    version = INF_VERSION
};

public APLRes AskPluginLoad2( Handle hPlugin, bool late, char[] szError, int error_len )
{
    // LIBRARIES
    RegPluginLibrary( INFLUX_LIB_SIMPLERANKS );
    
    
    CreateNative( "Influx_GetClientSimpleRank", Native_GetClientSimpleRank );
    CreateNative( "Influx_GetClientSimpleRankPoints", Native_GetClientSimpleRankPoints );
    
    
    g_bLate = late;
}

public void OnPluginStart()
{
    g_hRanks = new ArrayList( RANK_SIZE );
    g_hCoaches = new ArrayList(64);
    g_hTesters = new ArrayList(64);
    
    g_hMapRewards = new ArrayList( REWARD_SIZE );
    
    g_hModePoints = new ArrayList( P_SIZE );
    g_hStylePoints = new ArrayList( P_SIZE );
    //g_nMapReward = -1;
    
    
    // PRIVILEGE CMDS
    RegAdminCmd( INF_PRIVCOM_CUSTOMRANK, Cmd_Empty, ADMFLAG_ROOT );
    RegAdminCmd( INF_PRIVCOM_MAPREWARD, Cmd_Empty, ADMFLAG_ROOT );
    
    
    // ADMIN CMDS
    //RegAdminCmd( "sm_recalcranks", Cmd_Admin_RecalcRanks, ADMFLAG_ROOT );
    
    // CMDS
    RegConsoleCmd( "sm_title", Cmd_CustomRank );
    RegConsoleCmd( "sm_setmapreward", Cmd_SetMapReward );
    RegConsoleCmd( "sm_listranks", Cmd_ListRanksCmd );
    RegConsoleCmd( "sm_myrank", Cmd_MyRank);
    RegConsoleCmd( "sm_profile", Cmd_MyRank );
    RegConsoleCmd( "sm_p", Cmd_MyRank );
    RegConsoleCmd( "sm_topranks", Cmd_TopRanks );
    RegConsoleCmd( "sm_checkrank", Cmd_CheckRanks );
    RegConsoleCmd( "sm_finished", Cmd_FinishedMaps );
    RegConsoleCmd( "sm_unfinished", Cmd_UnfinishedMaps );
    RegConsoleCmd( "sm_otherranks", Cmd_OtherRanksCmd );
        
    if ( g_bLate )
    {
        int mapid = Influx_GetCurrentMapId();
        
        if ( mapid > 0 )
            Influx_OnMapIdRetrieved( mapid, false );
        
        for ( int i = 1; i <= MaxClients; i++ )
        {
            if ( !IsClientInGame( i ) ) continue;
            
            
            OnClientPutInServer( i );
            
            if ( !IsFakeClient( i ) )
            {
                int uid = Influx_GetClientId( i );
            
                if ( uid > 0 )
                    Influx_OnClientIdRetrieved( i, uid, false );
            }
        }
    }
}

public void OnMapStart()
{
    ReadRanks();
    ReadStyleModePoints();
    ReadCustomAuths();
    ReadCustomAuthsPost();
}

public void OnClientPutInServer( int client )
{
    g_nPoints[client] = 0;

    g_nPoints_NORMAL[client] = 0;
    g_nPoints_SW[client] = 0;
    g_nPoints_W[client] = 0;
    g_nPoints_AD[client] = 0;
    g_nPoints_HSW[client] = 0;
    g_nPoints_RHSW[client] = 0;
    g_nPoints_LOWGRAV[client] = 0;
    g_nPoints_BWD[client] = 0;
    g_nPoints_PARKOUR[client] = 0;
    g_nPoints_TAS[client] = 0;
    g_nPoints_PRESPEED[client] = 0;

    g_szCurRank[client][0] = 0;
    g_szDonRank[client][0] = 0;
    g_iCurRank[client] = -1;
    
    g_bChose[client] = false;
    scoreboardPlayerRankTimer[client] = CreateTimer(1.0, Timer_UpdatePlayerScoreboard, client, TIMER_REPEAT);
    g_lClientStyleID[client] = -1;    
}

public void OnClientDisconnect(int client)
{
    if( scoreboardPlayerRankTimer[client] != null )
    {
        KillTimer(scoreboardPlayerRankTimer[client]);
        scoreboardPlayerRankTimer[client] = null;
    }

    g_lClientStyleID[client] = -1;
}

public void OnAllPluginsLoaded()
{
    DB_Init();
}

public void Influx_OnRequestHelpCmds()
{
    Influx_AddHelpCommand( "sm_setmapreward <name (optional)> <reward>", "Set map's reward.", true );
}

public void Influx_OnMapIdRetrieved( int mapid, bool bNew )
{
    DB_InitMap( mapid );
}

public void Influx_OnClientIdRetrieved( int client, int uid, bool bNew )
{
    DB_InitClient( client );
}

public void Influx_OnTimerFinishPost( int client, int runid, int mode, int style, float time, float prev_pb, float prev_best, int flags )
{
    if ( GetMapRewardPointsSafe( runid ) == 0 )
    {
        return;
    }
    
    DB_CheckClientRecCount( client, runid, mode, style, time, prev_pb, prev_best );
}

stock int GetRankClosest( int points, bool bIgnoreUnlock = true )
{
    int closest_index = -1;
    
    decl closest_dif;
    
    decl p;
    decl dif;
    
    int len = g_hRanks.Length;
    for ( int i = 0; i < len; i++ )
    {
        p = g_hRanks.Get( i, RANK_POINTS );
        
        if ( bIgnoreUnlock && g_hRanks.Get( i, RANK_UNLOCK ) ) continue;
        
        if ( p > points ) continue;
        
        
        dif = points - p;
        
        if ( closest_index != -1 && closest_dif < dif )
        {
            continue;
        }
        
        closest_index = i;
        closest_dif = dif;
    }
    
    return closest_index;
}

stock int FindRankByName( const char[] szName )
{
    decl String:szTemp[MAX_RANK_SIZE];
    
    int len = g_hRanks.Length;
    for ( int i = 0; i < len; i++ )
    {
        g_hRanks.GetString( i, szTemp, sizeof( szTemp ) );
        
        if ( StrEqual( szName, szTemp ) )
        {
            return i;
        }
    }
    
    return -1;
}

stock int GetRankPoints( int index )
{
    if ( index == -1 ) return 133700;
    
    return g_hRanks.Get( index, RANK_POINTS );
}

stock void GetRankName( int index, char[] out, int len )
{
    if ( index == -1 ) return;
    
    
    g_hRanks.GetString( index, out, len );
}

stock void SetClientDefRank( int client )
{
    decl index;

    int style = Influx_GetClientStyle( client );
    if(style == 0)    
        g_nPoints[client] = g_nPoints_NORMAL[client];
    else if(style == 1)
        g_nPoints[client] = g_nPoints_SW[client];
    else if(style == 2)
        g_nPoints[client] = g_nPoints_W[client];
    else if(style == 3)
        g_nPoints[client] = g_nPoints_AD[client];
    else if(style == 4)
        g_nPoints[client] = g_nPoints_HSW[client];
    else if(style == 5)
        g_nPoints[client] = g_nPoints_RHSW[client];
    else if(style == 6)
        g_nPoints[client] = g_nPoints_LOWGRAV[client];
    else if(style == 7)
        g_nPoints[client] = g_nPoints_BWD[client];
    else if(style == 8)
        g_nPoints[client] = g_nPoints_PARKOUR[client];
    else if(style == 9)
        g_nPoints[client] = g_nPoints_TAS[client];
    else if(style == 10)
        g_nPoints[client] = g_nPoints_PRESPEED[client];    
    
    index = GetRankClosest( g_nPoints[client] );
    if ( index == -1 )
    {
        index = GetRankClosest( g_nPoints[client], false );
        if ( index == -1 ) return;
    }
    

    SetClientRank( client, index, false );
}

stock void SetClientRank( int client, int index, bool bChose, const char[] szOver = "", bool bPrint = false )
{
    if ( index == -1 )
    {
        strcopy( g_szDonRank[client], sizeof( g_szDonRank[] ), szOver );        
    	Influx_PrintToChat( _, client, "Your rank is now '{MAINCLR1}%s{CHATCLR}'!", g_szDonRank[client] );
    }
    else
    {
        GetRankName( index, g_szCurRank[client], sizeof( g_szCurRank[] ) );
        if ( bPrint )
        {
            Influx_PrintToChat( _, client, "Your rank is now '{MAINCLR1}%s{CHATCLR}'!", g_szCurRank[client] );
        }
    }
    
    g_bChose[client] = bChose;
    g_iCurRank[client] = index;
}

stock void RewardClient(int client,
                        int runid,
                        int mode,
                        int style,
                        int override_reward = -1,
                        int database_reward = -1,
                        bool bFirst = true )
{
    int reward;
    
    if ( override_reward == 0 )
    {
        reward = 0;
    }
    else
    {
        // Use override reward.
        reward = override_reward;
    }
    
    int oldrank = 0;
    int newrank = 0;

    if ( reward != 0 ) {

        if(style == 0){
            oldrank = GetRankClosest(g_nPoints_NORMAL[client]); 
            newrank = GetRankClosest(g_nPoints_NORMAL[client] + reward);
            g_nPoints_NORMAL[client]+=reward;
            g_nPoints[client] = g_nPoints_NORMAL[client];
            if(g_nBestPointsStyle[STYLE_NORMAL] < newrank)
            {
                g_nBestPointsStyle[STYLE_NORMAL] = newrank
                g_nBestPointsUser[STYLE_NORMAL] = Influx_GetClientId(client);
                char szUsername[64];
                GetClientName( client, szUsername, 64);
                g_nBestPointsUserName[STYLE_NORMAL] = szUsername;
            }
        }
        else if(style == 1){
            oldrank = GetRankClosest(g_nPoints_SW[client]); 
            newrank = GetRankClosest(g_nPoints_SW[client] + reward);
            g_nPoints_SW[client]+=reward;
            g_nPoints[client] = g_nPoints_SW[client];
            if(g_nBestPointsStyle[STYLE_SW] < newrank)
            {
                g_nBestPointsStyle[STYLE_SW] = newrank
                g_nBestPointsUser[STYLE_SW] = Influx_GetClientId(client);
                char szUsername[64];
                GetClientName( client, szUsername, 64);
                g_nBestPointsUserName[STYLE_SW] = szUsername;
            }            
        }
        else if(style == 2){
            oldrank = GetRankClosest(g_nPoints_W[client]); 
            newrank = GetRankClosest(g_nPoints_W[client] + reward);
            g_nPoints_W[client]+=reward;
            g_nPoints[client] = g_nPoints_W[client];
            if(g_nBestPointsStyle[STYLE_W] < newrank)
            {
                g_nBestPointsStyle[STYLE_W] = newrank
                g_nBestPointsUser[STYLE_W] = Influx_GetClientId(client);
                char szUsername[64];
                GetClientName( client, szUsername, 64);
                g_nBestPointsUserName[STYLE_W] = szUsername;
            }            
        }
        else if(style == 3){
            oldrank = GetRankClosest(g_nPoints_AD[client]); 
            newrank = GetRankClosest(g_nPoints_AD[client] + reward);
            g_nPoints_AD[client]+=reward;
            g_nPoints[client] = g_nPoints_AD[client];
            if(g_nBestPointsStyle[STYLE_AD] < newrank)
            {
                g_nBestPointsStyle[STYLE_AD] = newrank
                g_nBestPointsUser[STYLE_AD] = Influx_GetClientId(client);
                char szUsername[64];
                GetClientName( client, szUsername, 64);
                g_nBestPointsUserName[STYLE_AD] = szUsername;
            }            
        }
        else if(style == 4){
            oldrank = GetRankClosest(g_nPoints_HSW[client]); 
            newrank = GetRankClosest(g_nPoints_HSW[client] + reward);
            g_nPoints_HSW[client]+=reward;
            g_nPoints[client] = g_nPoints_HSW[client];
            if(g_nBestPointsStyle[STYLE_HSW] < newrank)
            {
                g_nBestPointsStyle[STYLE_HSW] = newrank
                g_nBestPointsUser[STYLE_HSW] = Influx_GetClientId(client);
                char szUsername[64];
                GetClientName( client, szUsername, 64);
                g_nBestPointsUserName[STYLE_HSW] = szUsername;
            }            
        }
        else if(style == 5){
            oldrank = GetRankClosest(g_nPoints_RHSW[client]); 
            newrank = GetRankClosest(g_nPoints_RHSW[client] + reward);
            g_nPoints_RHSW[client]+=reward;
            g_nPoints[client] = g_nPoints_RHSW[client];
            if(g_nBestPointsStyle[STYLE_RHSW] < newrank)
            {
                g_nBestPointsStyle[STYLE_RHSW] = newrank
                g_nBestPointsUser[STYLE_RHSW] = Influx_GetClientId(client);
                char szUsername[64];
                GetClientName( client, szUsername, 64);
                g_nBestPointsUserName[STYLE_RHSW] = szUsername;
            }            
        }
        else if(style == 6){
            oldrank = GetRankClosest(g_nPoints_LOWGRAV[client]); 
            newrank = GetRankClosest(g_nPoints_LOWGRAV[client] + reward);
            g_nPoints_LOWGRAV[client]+=reward;
            g_nPoints[client] = g_nPoints_LOWGRAV[client];
            if(g_nBestPointsStyle[STYLE_LOWGRAV] < newrank)
            {
                g_nBestPointsStyle[STYLE_LOWGRAV] = newrank
                g_nBestPointsUser[STYLE_LOWGRAV] = Influx_GetClientId(client);
                char szUsername[64];
                GetClientName( client, szUsername, 64);
                g_nBestPointsUserName[STYLE_LOWGRAV] = szUsername;
            }            
        }
        else if(style == 7){
            oldrank = GetRankClosest(g_nPoints_BWD[client]); 
            newrank = GetRankClosest(g_nPoints_BWD[client] + reward);
            g_nPoints_BWD[client]+=reward;
            g_nPoints[client] = g_nPoints_BWD[client];
            if(g_nBestPointsStyle[STYLE_BWD] < newrank)
            {
                g_nBestPointsStyle[STYLE_BWD] = newrank
                g_nBestPointsUser[STYLE_BWD] = Influx_GetClientId(client);
                char szUsername[64];
                GetClientName( client, szUsername, 64);
                g_nBestPointsUserName[STYLE_BWD] = szUsername;
            }            
        }
        else if(style == 8){
            oldrank = GetRankClosest(g_nPoints_PARKOUR[client]); 
            newrank = GetRankClosest(g_nPoints_PARKOUR[client] + reward);
            g_nPoints_PARKOUR[client]+=reward;
            g_nPoints[client] = g_nPoints_PARKOUR[client];
            if(g_nBestPointsStyle[STYLE_PARKOUR] < newrank)
            {
                g_nBestPointsStyle[STYLE_PARKOUR] = newrank
                g_nBestPointsUser[STYLE_PARKOUR] = Influx_GetClientId(client);
                char szUsername[64];
                GetClientName( client, szUsername, 64);
                g_nBestPointsUserName[STYLE_PARKOUR] = szUsername;
            }            
        }
        else if(style == 9){
            oldrank = GetRankClosest(g_nPoints_TAS[client]); 
            newrank = GetRankClosest(g_nPoints_TAS[client] + reward);
            g_nPoints_TAS[client]+=reward;
            g_nPoints[client] = g_nPoints_TAS[client];
            if(g_nBestPointsStyle[STYLE_TAS] < newrank)
            {
                g_nBestPointsStyle[STYLE_TAS] = newrank
                g_nBestPointsUser[STYLE_TAS] = Influx_GetClientId(client);
                char szUsername[64];
                GetClientName( client, szUsername, 64);
                g_nBestPointsUserName[STYLE_TAS] = szUsername;
            }            
        }
        else if(style == 10){
            oldrank = GetRankClosest(g_nPoints_PRESPEED[client]); 
            newrank = GetRankClosest(g_nPoints_PRESPEED[client] + reward);
            g_nPoints_PRESPEED[client]+=reward;
            g_nPoints[client] = g_nPoints_PRESPEED[client];
            if(g_nBestPointsStyle[STYLE_PRESPEED] < newrank)
            {
                g_nBestPointsStyle[STYLE_PRESPEED] = newrank
                g_nBestPointsUser[STYLE_PRESPEED] = Influx_GetClientId(client);
                char szUsername[64];
                GetClientName( client, szUsername, 64);
                g_nBestPointsUserName[STYLE_PRESPEED] = szUsername;
            }            
        }

        if ( true && reward > 0)
        {
            Influx_PrintToChat( _, client, "You've received {MAINCLR1}%i{CHATCLR} points! You now have {MAINCLR1}%i{CHATCLR} points!", reward, g_nPoints[client] );
        } else if( true && reward < 0)
        {
            Influx_PrintToChat( _, client, "You've lost {MAINCLR1}%i{CHATCLR} points! You now have {MAINCLR1}%i{CHATCLR} points!", reward*-1, g_nPoints[client] );
        }
        
        if ( oldrank != newrank && newrank != -1 )
        {
            // Update their rank.
            if ( !g_bChose[client] )
            {
                SetClientRank( client, newrank, false, _, true );
            }
        }
        DB_IncClientPoints( client, runid, mode, style, reward, database_reward, bFirst );
    
    }   
}

stock bool IsValidReward( int reward, int issuer = 0, bool bPrint = false )
{
    if ( reward < 0 )
    {
        if ( bPrint )
        {
            Inf_ReplyToClient( issuer, "Reward cannot be negative!" );
        }
        
        return false;
    }
    
    return true;
}

stock void SetCurrentMapReward( int issuer, int runid, int reward )
{
    if ( !IsValidReward( reward, issuer, true ) ) return;
    
    
    SetMapReward( runid, reward );
    
    DB_UpdateMapReward( Influx_GetCurrentMapId(), runid, reward );
    
    char szRun[32];
    Influx_GetRunName( runid, szRun, sizeof( szRun ) );
    
    Inf_ReplyToClient( issuer, "Set current map's {MAINCLR1}%s{CHATCLR} reward to {MAINCLR1}%i{CHATCLR} points.",
        szRun,
        reward );
}

stock bool CanUserUseCustomRank( int client )
{
    char actualID[64];
    GetClientAuthId(client, AuthId_Steam2, actualID, sizeof(actualID));
    // MERCY, DREWERTH   
    if(StrEqual(actualID, "STEAM_1:0:55407526") || StrEqual(actualID, "STEAM_1:1:35654645"))
    {
        return true;
    }

    return CheckCommandAccess( client, INF_PRIVCOM_CUSTOMRANK, ADMFLAG_CUSTOM3, true );
}

stock bool CanUserSetMapReward( int client )
{
    return CheckCommandAccess( client, INF_PRIVCOM_MAPREWARD, ADMFLAG_ROOT );
}

stock int SetMapReward( int runid, int points )
{

    int index = FindMapRewardById( runid );
    if ( index != -1 )
    {
        g_hMapRewards.Set( index, points, REWARD_POINTS );
        return index;
    }
    
    decl data[REWARD_SIZE];
    
    data[REWARD_RUN_ID] = runid;
    data[REWARD_POINTS] = points;
    
    
    return g_hMapRewards.PushArray( data );
}

stock int FindMapRewardById( int runid )
{
    int len = g_hMapRewards.Length;
    for ( int i = 0; i < len; i++ )
    {
        if ( g_hMapRewards.Get( i, REWARD_RUN_ID ) == runid )
        {
            return i;
        }
    }
    
    return -1;
}

stock int GetMapRewardPointsSafe( int runid )
{
    int reward = GetMapRewardPoints( runid );
    
    // Must set custom amount for bonuses.
    if ( runid != MAIN_RUN_ID && reward < 0 ) return 0;
    
    
    return ( reward < 0 ) ? 1 : reward;
}

stock int GetMapRewardPoints( int runid )
{
    int index = FindMapRewardById( runid );
    
    if ( index == -1 ) return -1;
    
    
    return g_hMapRewards.Get( index, REWARD_POINTS );
}

stock int GetModePoints( int mode )
{
    decl String:sz[32];
    Influx_GetModeSafeName( mode, sz, sizeof( sz ) );
    int index = FindMultById( mode, sz, g_hModePoints );
    
    if ( index == -1 ) return 0;
    
    
    return g_hModePoints.Get( index, P_VAL );
}

stock int GetStylePoints( int style )
{
    decl String:sz[32];
    Influx_GetStyleSafeName( style, sz, sizeof( sz ) );
    int index = FindMultById( style, sz, g_hStylePoints );
    
    if ( index == -1 ) return 0;
    
    
    return g_hStylePoints.Get( index, P_VAL );
}

stock int FindMultById( int id, const char[] sz, ArrayList array )
{
    decl myid;
    decl String:szTemp[32];
    
    
    int len = array.Length;
    for ( int i = 0; i < len; i++ )
    {
        myid = array.Get( i, P_ID );
        
        if ( myid != -1 )
        {
            if ( myid == id ) return i;
        }
        else
        {
            array.GetString( i, szTemp, sizeof( szTemp ) );
            
            if ( StrEqual( sz, szTemp, false ) )
                return i;
        }
    }
    
    return -1;
}

stock int CalcReward( int runid, int mode, int style, bool bFirst, float time, float prev_pb, float prev_best )
{
    float reward = float(GetMapRewardPointsSafe( runid ));
    float remain = float(GetMapRewardPointsSafe( runid ));
    
    if ( reward < 1.0 ) return 0;
    
    
    if ( !bFirst )
    {
        reward = reward / 4.0;
        remain = remain - reward;
        float value = ((prev_best / time) * (remain));
        reward = reward + value;
    }
    
    if( RoundToFloor(reward) > float(GetMapRewardPointsSafe( runid )))
    {
        reward = float(GetMapRewardPointsSafe( runid ));
    }
    return RoundToFloor(reward) + GetModePoints( mode ) + GetStylePoints( style );
}

// NATIVES
public int Native_GetClientSimpleRank( Handle hPlugin, int nParms )
{
    int client = GetNativeCell( 1 );
    
    SetNativeString( 2, g_szCurRank[client], GetNativeCell( 3 ) );
    
    return 1;
}

public int Native_GetClientSimpleRankPoints( Handle hPlugin, int nParms )
{
    int client = GetNativeCell( 1 );
    
    return g_nPoints[client];
}

public Action Timer_UpdatePlayerScoreboard(Handle timer, any client)
{
    if( !client ) return Plugin_Continue;
    if( !IsClientInGame( client ) ) return Plugin_Continue;
    if( IsFakeClient( client ) ) return Plugin_Continue;
    if( IsClientReplay( client ) ) return Plugin_Continue;

    if(Influx_GetClientStyle(client) != g_lClientStyleID[client])
    {
        SetClientDefRank(client);
    }


    char szRank[64];
    char szStyle[16];
    char szFinal[80];

    if( GetClientTeam( client ) == CS_TEAM_SPECTATOR ) 
    {
        CS_SetClientClanTag(client, "");
        return Plugin_Continue;
    } 

    if(CanUserUseCustomRank( client ) && g_szDonRank[client][0] != 0)
    {
        FormatEx(szFinal, sizeof(szFinal), "[%s]", g_szDonRank[client]);
        CS_SetClientClanTag(client, szFinal);    
        return Plugin_Continue;                
    }
    
    Influx_GetStyleShortName( Influx_GetClientStyle(client), szStyle, sizeof(szStyle), false );
    Influx_GetClientSimpleRank( client, szRank, sizeof(szRank) );
    Influx_RemoveChatColors(szRank, sizeof(szRank) );

    FormatEx(szFinal, sizeof(szFinal), "[%s-%s]", szStyle, szRank);
    CS_SetClientClanTag(client, szFinal);
    
    return Plugin_Continue;
}