#pragma semicolon 1

#include <sourcemod>
#include <sdkhooks>
#include <sdktools>
#include <influx/core>

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Disable Molotov",
    description = "",
    version = INF_VERSION
};

public OnClientPutInServer(client)
{
	SDKHook(client, SDKHook_WeaponEquip, OnPickup);
}

public OnClientDisconnect(client) 
{ 
	if(IsClientInGame(client)) 
	{ 
		SDKUnhook(client, SDKHook_WeaponEquip, OnPickup);
	} 
} 

public Action:OnPickup(client, weapon) 
{ 
	decl String:str[32]; 
	GetEdictClassname(weapon, str, sizeof(str)); 
  
	if(StrEqual(str, "weapon_incgrenade") || StrEqual(str, "weapon_molotov")) 
	{ 
		return Plugin_Handled;
	} 

	return Plugin_Continue; 
}  