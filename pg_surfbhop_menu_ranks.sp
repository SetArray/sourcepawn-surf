#include <sourcemod>

#include <influx/core>
#include <influx/help>

#include <msharedutil/arrayvec>

float g_flLastCmdO[INF_MAXPLAYERS];

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Ranks Menu",
    description = "",
    version = INF_VERSION
};

public void OnPluginStart()
{
    // CMDS
    RegConsoleCmd( "sm_rank", Cmd_Ranks );
    RegConsoleCmd( "sm_ranks", Cmd_Ranks );
}

public void OnClientPutInServer( int client )
{
    g_flLastCmdO[client] = 0.0;
}

public void Influx_RequestHelpCmds()
{
    Influx_AddHelpCommand( "ranks", "Opens the ranks menu." );
}

public Action Cmd_Ranks( int client, int args )
{  
    if ( IS_ENT_PLAYER( client ) )
    {
        if ( Inf_HandleCmdSpam( client, 3.0, g_flLastCmdO[client], true ) )
        {
            return Plugin_Handled;
        }
                    
        Menu menu = new Menu( Hndlr_RankMenu );
        menu.SetTitle( "Rank Commands: " );

        menu.AddItem("listranks", "List Ranks"); //
        menu.AddItem("myrank ", "My Rank"); //
        menu.AddItem("topranks", "Top Ranks"); 
        menu.AddItem("checkrank", "Check Ranks"); //
        menu.AddItem("title", "VIP Custom Title"); // 
        
        menu.ExitButton = true;
        menu.Display( client, MENU_TIME_FOREVER );
    }
    
    return Plugin_Handled;
}

public int Hndlr_RankMenu( Menu menu, MenuAction action, int client, int index )
{
    if(action == MenuAction_Select)
    {
        char selected[32];
        menu.GetItem(index, selected, sizeof(selected));
        FakeClientCommand(client, "sm_%s", selected);
    }    
    else if(action == MenuAction_End)
    {
        delete menu;
    }
    return 0;
}