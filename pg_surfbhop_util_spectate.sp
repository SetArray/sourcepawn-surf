#include <sourcemod>
#include <clientprefs>
#include <influx/core>

#include <msharedutil/ents>

#undef REQUIRE_PLUGIN
#include <influx/options>

Handle g_hSpectateListTimer[MAXPLAYERS+1];
Handle g_hCookie_ShowSpecs;
int g_fShowSpecs[MAXPLAYERS+1];

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Spectator List",
    description = "",
    version = INF_VERSION
};

public void OnPluginStart()
{   
    if ( (g_hCookie_ShowSpecs = RegClientCookie( "influx_speclist", INF_NAME..." Show Spectators", CookieAccess_Protected )) == null )
    {
        SetFailState( INF_CON_PRE..."Couldn't register speclist cookie!" );
    }

    // CMDS
    RegConsoleCmd( "sm_speclist", Cmd_Spectators );
}

public void Influx_OnRequestOptionsCmds()
{
    Influx_AddOptionsCommand( "speclist", "Toggle your spectator list ON/OFF." );
}

public void OnClientPutInServer(int client)
{
    g_hSpectateListTimer[client] = CreateTimer(1.0, Timer_UpdateSpectateList, client, TIMER_REPEAT);
}

public void OnClientDisconnect(int client)
{
    if(g_hSpectateListTimer[client] != null)
    {
        KillTimer(g_hSpectateListTimer[client]);
        g_hSpectateListTimer[client] = null;
    }

    if(IsFakeClient(client)) return;

    char szCookie[12];
    IntToString(g_fShowSpecs[client], szCookie, sizeof(szCookie));
    SetClientCookie(client, g_hCookie_ShowSpecs, szCookie);
}

public Action Timer_UpdateSpectateList(Handle timer, any client)
{
    if( !client ) return Plugin_Continue;
    if( !IsClientInGame( client ) ) return Plugin_Continue;
    if( IsFakeClient( client ) ) return Plugin_Continue;
    if( IsClientReplay( client ) ) return Plugin_Continue;
    if( g_fShowSpecs[client] != 1) return Plugin_Continue;
    if( GetClientMenu(client) != MenuSource_None && GetClientMenu(client) != MenuSource_RawPanel) return Plugin_Continue;

    int activeSpectators = 0;
    int activeSpectateID[5];
    for(new cl = 1; cl <= MaxClients; cl++)
    {
        if(IsClientInGame(cl) && IsClientObserver(cl) && !IsFakeClient(cl))
        {
            int sMode = GetEntProp(cl, Prop_Send, "m_iObserverMode");
            if(sMode == 4 || sMode == 5)
            {
                int sTarg = GetEntPropEnt(cl, Prop_Send, "m_hObserverTarget");
                if(sTarg == client && activeSpectators < 5)
                {
                    activeSpectateID[activeSpectators] = cl;
                    activeSpectators++;
                }
                else if(activeSpectators == 5)
                {
                    break;
                }
            }
        }
    }

    if(activeSpectators == 0) return Plugin_Continue;

    Panel panel = new Panel();

    char szTitle[24];
    FormatEx(szTitle, sizeof(szTitle), "Active Spectators: %d", activeSpectators);
    panel.SetTitle(szTitle);
    panel.DrawItem( "", ITEMDRAW_SPACER );

    for(new cl = 0; cl < activeSpectators; cl++)
    {
        char szName[24];
        char szNameTemp[40];
        GetClientName(activeSpectateID[cl], szNameTemp, sizeof(szNameTemp));
        if(strlen(szNameTemp) > 23)
        {
            FormatEx(szName, 20, "%s...", szNameTemp[0]);
        }
        else
        {
            FormatEx(szName, sizeof(szName), "%s", szNameTemp)
        }
        panel.DrawText(szName);
    }
    panel.DrawItem( "", ITEMDRAW_SPACER );
    panel.Send( client, Hndlr_Panel_Empty, 1 );
    delete panel;

    return Plugin_Continue;
}

public void OnClientCookiesCached(int client)
{
    if(AreClientCookiesCached(client))
    {
        char szCookie[12];
        GetClientCookie(client, g_hCookie_ShowSpecs, szCookie, sizeof(szCookie));

        if(szCookie[0] != '\0')
        {
            g_fShowSpecs[client] = StringToInt(szCookie);
        }
        else
        {
            g_fShowSpecs[client] = 1;
        }
    }
    else
    {
        g_fShowSpecs[client] = 1;
    }
}

public Action Cmd_Spectators(int client, int args)
{
    if(!client) return Plugin_Handled;

    if(g_fShowSpecs[client] != 1)
    {
        g_fShowSpecs[client] = 1;
    } 
    else
    {
        g_fShowSpecs[client] = 0;
    }

    return Plugin_Handled;
}

public int Hndlr_Panel_Empty( Menu menu, MenuAction action, int client, int param2 ) {}
