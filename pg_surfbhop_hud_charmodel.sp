#include <sourcemod>
#include <clientprefs>

#include <influx/core>
#include <influx/hud>

#undef REQUIRE_PLUGIN
#include <influx/options>

Handle g_hCookie_HidePlayers;
int g_fHidePlayers[MAXPLAYERS+1];

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Model Handler",
    description = "",
    version = INF_VERSION
};

public void OnPluginStart()
{
    if ( (g_hCookie_HidePlayers = RegClientCookie( "influx_hideplayers", INF_NAME..." Hide Players", CookieAccess_Protected )) == null )
    {
        SetFailState( INF_CON_PRE..."Couldn't register hideplayers cookie!" );
    }

    RegConsoleCmd( "sm_hide", Cmd_Hide );
}

public void Influx_OnRequestOptionsCmds()
{
    Influx_AddOptionsCommand( "hide", "Toggles other player models ON/OFF." );
}

public void OnClientPutInServer( int client )
{
    // Has to be hooked to everybody.
    SDKHook( client, SDKHook_SetTransmit, E_SetTransmit_Client );
}

public void OnClientDisconnect( int client )
{
    SDKUnhook( client, SDKHook_SetTransmit, E_SetTransmit_Client );

    if(IsFakeClient(client)) return;

    char szCookie[12];
    IntToString(g_fHidePlayers[client], szCookie, sizeof(szCookie));
    SetClientCookie(client, g_hCookie_HidePlayers, szCookie);
}

public void OnClientCookiesCached(int client)
{
    if(AreClientCookiesCached(client))
    {
        char szCookie[12];
        GetClientCookie(client, g_hCookie_HidePlayers, szCookie, sizeof(szCookie));

        if(szCookie[0] != '\0')
        {
            g_fHidePlayers[client] = StringToInt(szCookie);
        }
        else
        {
            g_fHidePlayers[client] = 0;
        }
    }
    else
    {
        g_fHidePlayers[client] = 0;
    }
}

public Action Cmd_Hide(int client, int args)
{
    if(!client) return Plugin_Handled;

    if(g_fHidePlayers[client] != 1)
    {
        g_fHidePlayers[client] = 1;
    } 
    else
    {
        g_fHidePlayers[client] = 0;
    }

    return Plugin_Handled;    
}

public Action E_SetTransmit_Client( int ent, int client )
{
    if ( !IS_ENT_PLAYER( ent ) || client == ent ) return Plugin_Continue;
    
    // If we're spectating somebody, show them no matter what.
    if ( !IsPlayerAlive( client ) && GetClientObserverTarget( client ) == ent )
    {
        return Plugin_Continue;
    }
    
    
    if ( IsFakeClient( ent ) )
    {
        return ( g_fHidePlayers[client] == 1 ) ? Plugin_Handled : Plugin_Continue;
    }
    else
    {
        return ( g_fHidePlayers[client] == 1 ) ? Plugin_Handled : Plugin_Continue;
    }
}

public void OnGameFrame()
{
    for (int i = 1; i <= MaxClients; i++) //loop players.  
    {  
        if (i > 0 && IsClientInGame(i) && !IsFakeClient(i)) //if players found and ingame continue.  
        {  
            SetEntityRenderMode(i, RENDER_TRANSCOLOR); 
            SetEntityRenderColor(i, 0,0,0,190); 
        }  
        //else
        //{
        //    SetEntityRenderMode(i, RENDER_TRANSCOLOR); 
        //    SetEntityRenderColor(i, 255,69,0,190); //lol 69           
        //}
    }     
}
