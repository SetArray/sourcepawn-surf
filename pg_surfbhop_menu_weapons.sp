#include <sourcemod>
#include <cstrike>
#include <sdktools>
#include <smlib/weapons>

#include <influx/core>
#include <influx/stocks_core>

#include <msharedutil/ents>

#undef REQUIRE_PLUGIN
#include <influx/options>

enum
{
	SLOT_PRIMARY,
	SLOT_SECONDARY,
	SLOT_MELEE
};

float g_flLastAllowed[INF_MAXPLAYERS];
int g_Offset_hMyWeapons;

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Weapon Manager",
    description = "",
    version = INF_VERSION
};

public void OnPluginStart()
{
    if ( (g_Offset_hMyWeapons = FindSendPropInfo( "CCSPlayer", "m_hMyWeapons" )) == -1 )
    {
        SetFailState( INF_CON_PRE..."Couldn't find offset for m_hMyWeapons!" );
    }

    RegConsoleCmd( "sm_knife", Cmd_Knife ); 
    RegConsoleCmd( "sm_usp", Cmd_Usp );
    RegConsoleCmd( "sm_glock", Cmd_Glock );
    RegConsoleCmd( "sm_deagle", Cmd_Deagle );
    RegConsoleCmd( "sm_scout", Cmd_Scout );
    RegConsoleCmd( "sm_p90", Cmd_P90 );
    RegConsoleCmd( "sm_drop", Cmd_Drop ); 

    RegConsoleCmd( "sm_guns", Cmd_Weapons_Menu );
    RegConsoleCmd( "sm_gun", Cmd_Weapons_Menu );
    RegConsoleCmd( "sm_weapons", Cmd_Weapons_Menu );
    RegConsoleCmd( "sm_weapon", Cmd_Weapons_Menu );   
}

public void Influx_RequestOptionsCmds()
{
    Influx_AddOptionsCommand( "weapons", "Opens weapon select menu." );
}

public Action Cmd_Weapons_Menu(int client, int args)
{
    if(!client) return Plugin_Handled;

    Menu menu = new Menu(Hndlr_WepMenu);
    menu.SetTitle("Weapons Menu:\n ");

    menu.AddItem("drop", "Drop Weapons");
    menu.AddItem("knife", "Knife");
    menu.AddItem("usp", "USP-S");
    menu.AddItem("glock", "Glock-18");
    menu.AddItem("deagle", "Desert Eagle");
    menu.AddItem("scout", "Scout Sniper");
    menu.AddItem("p90", "Pro 90 SMG");
        
    menu.ExitButton = true;
    menu.Display(client, MENU_TIME_FOREVER);    

    return Plugin_Handled;
}

public int Hndlr_WepMenu( Menu menu, MenuAction action, int client, int index )
{
    if(action == MenuAction_Select)
    {
        char selected[32];
        menu.GetItem(index, selected, sizeof(selected));
        FakeClientCommand(client, "sm_%s", selected);
    }    
    else if(action == MenuAction_End)
    {
        delete menu;
    }
    return 0;
}

public Action Cmd_P90( int client, int args )
{
    GiveWeapon( client, "weapon_p90", SLOT_PRIMARY );
    return Plugin_Handled;
}

public Action Cmd_Scout( int client, int args )
{
    GiveWeapon( client, "weapon_ssg08", SLOT_PRIMARY );
    return Plugin_Handled;
}

public Action Cmd_Deagle( int client, int args )
{
    GiveWeapon( client, "weapon_deagle", SLOT_SECONDARY );
    return Plugin_Handled;
}

public Action Cmd_Glock( int client, int args )
{
    GiveWeapon( client, "weapon_glock", SLOT_SECONDARY );
    return Plugin_Handled;
}

public Action Cmd_Usp( int client, int args )
{
    GiveWeapon( client, "weapon_usp_silencer", SLOT_SECONDARY );
    return Plugin_Handled;
}

public Action Cmd_Knife( int client, int args )
{
    GiveWeapon( client, "weapon_knife", SLOT_MELEE );
    return Plugin_Handled;
}

stock void GiveWeapon( int client, const char[] wepname, int slot )
{
    if ( !client ) return;
    
    if ( !IsPlayerAlive( client ) ) return;
    
    if ( Inf_HandleCmdSpam( client, 1.0, g_flLastAllowed[client], true ) )
    {
        return;
    }
    
    
    int wep;
    
    if ( (wep = GetPlayerWeaponSlot( client, slot )) > 0 && IsValidEdict( wep ) )
    {
        RemovePlayerItem( client, wep );
    }
    
    
    GivePlayerItem( client, wepname );
    OnWeapon(client);
    
    
    // Equipping the weapon will for some reason delete it.
    //if ( wep > 0 ) EquipPlayerWeapon( client, wep );
}

public Action Cmd_Drop( int client, int args )
{
    if ( client && IsPlayerAlive( client ) )
    {
        if ( Inf_HandleCmdSpam( client, 1.0, g_flLastAllowed[client], true ) )
        {
            return Plugin_Handled;
        }
        
        
        int wep;
        for ( int i = 0; i < 128; i += 4 )
        {
            if ( (wep = GetEntDataEnt2( client, g_Offset_hMyWeapons + i )) > 0 )
            {
                RemovePlayerItem( client, wep );
            }
        }
    }
    
    return Plugin_Handled;
}

public void OnClientPutInServer( int client )
{
    Inf_SDKHook( client, SDKHook_WeaponDropPost, E_WeaponDropPost_Client );
    g_flLastAllowed[client] = 0.0;
}

public void OnClientDisconnect( int client )
{
    SDKUnhook( client, SDKHook_WeaponDropPost, E_WeaponDropPost_Client );
}

public void E_WeaponDropPost_Client( int client, int weapon )
{
    if ( weapon < 1 ) return;
    
    if ( !IsValidEntity( weapon ) ) return;
    
    
    if ( !KillEntity( weapon ) )
    {
        decl String:wep[32];
        GetEntityClassname( weapon, wep, sizeof( wep ) );
        
        LogError( INF_CON_PRE..."Couldn't delete weapon %s (%i)!", wep, weapon );
    }
}

public OnWeapon(client)
{
	new Primary = GetPlayerWeaponSlot(client, CS_SLOT_PRIMARY);
	new Secondary = GetPlayerWeaponSlot(client, CS_SLOT_SECONDARY);

	if(Primary != -1)
	{
		Weapon_SetPrimaryClip(Primary, 0);
		ClearAmmo(client, Primary);
	}

	if (Secondary != -1)
	{
		Weapon_SetPrimaryClip(Secondary, 0);
		ClearAmmo(client, Secondary);
	}
}  

stock ClearAmmo(client, weapon)
{
	new ammotype = GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType");
	if(ammotype == -1) return;

	SetEntProp(client, Prop_Send, "m_iAmmo", 0, _, ammotype);
	SetEntProp(weapon, Prop_Send, "m_iPrimaryReserveAmmoCount", 0);
	SetEntProp(weapon, Prop_Send, "m_iClip1", 0);
}