#if defined _influx_options_included
    #endinput
#endif

#define _influx_options_included
#define INFLUX_LIB_OPTIONS             "influx_options"

// Waiting to be removed.
#define Influx_RequestOptionsCmds      Influx_OnRequestOptionsCmds

forward void Influx_OnRequestOptionsCmds();

native bool Influx_AddOptionsCommand( const char[] szCommand, const char[] szMsg );
native bool Influx_RemoveOptionsCommand( const char[] szCommand );

public SharedPlugin __pl_influx_options =
{
    name = INFLUX_LIB_OPTIONS,
    file = INFLUX_LIB_OPTIONS...".smx",
#if defined INFLUX_LIB_OPTIONS
    required = 1
#else
    required = 0
#endif
};

#if !defined REQUIRE_PLUGIN
public void __pl_influx_options_SetNTVOptional()
{
    MarkNativeAsOptional( "Influx_AddOptionsCommand" );
    MarkNativeAsOptional( "Influx_RemoveOptionsCommand" );
}
#endif