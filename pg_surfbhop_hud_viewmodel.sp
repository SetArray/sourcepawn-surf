#include <sourcemod>
#include <clientprefs>
#include <cstrike>

#include <influx/core>

#undef REQUIRE_PLUGIN
#include <influx/options>

Handle g_hCookie_ShowModel;
int g_fShowModel[MAXPLAYERS+1];

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Viewmodel",
    description = "",
    version = INF_VERSION
};

public void OnPluginStart()
{
    // EVENTS
    HookEvent( "player_spawn", E_PlayerSpawn );

    if ( (g_hCookie_ShowModel = RegClientCookie( "influx_showmodel", INF_NAME..." Show Models", CookieAccess_Protected )) == null )
    {
        SetFailState( INF_CON_PRE..."Couldn't register showmodel cookie!" );
    }    
    
    // CMDS
    RegConsoleCmd( "sm_viewmodel", Cmd_Viewmodel );
}

public void Influx_OnRequestOptionsCmds()
{
    Influx_AddOptionsCommand( "viewmodel", "Toggle your viewmodel ON/OFF." );
}

public void OnClientDisconnect(int client)
{
    if(IsFakeClient(client)) return;

    char szCookie[12];
    IntToString(g_fShowModel[client], szCookie, sizeof(szCookie));
    SetClientCookie(client, g_hCookie_ShowModel, szCookie);    
}

public void E_PlayerSpawn( Event event, const char[] szEvent, bool bImUselessWhyDoIExist )
{
    int client = GetClientOfUserId( event.GetInt( "userid" ) );
    if ( !client ) return;
    
    if ( GetClientTeam( client ) <= CS_TEAM_SPECTATOR || !IsPlayerAlive( client ) ) return;
    
    
    if ( !IsFakeClient( client ) )
    {
        RequestFrame( E_PlayerSpawn_Delay, GetClientUserId( client ) );
    }
}

public void E_PlayerSpawn_Delay( int client )
{
    if ( !(client = GetClientOfUserId( client )) ) return;
    
    if ( !IsPlayerAlive( client ) ) return;
    
    
    SetDrawViewmodel( client, ( g_fShowModel[client] != 1 ) ? false : true );
}

public void OnClientCookiesCached(int client)
{
    if(AreClientCookiesCached(client))
    {
        char szCookie[12];
        GetClientCookie(client, g_hCookie_ShowModel, szCookie, sizeof(szCookie));

        if(szCookie[0] != '\0')
        {
            g_fShowModel[client] = StringToInt(szCookie);
            SetDrawViewmodel( client, ( g_fShowModel[client] != 1 ) ? false : true );
        }
        else
        {
            g_fShowModel[client] = 1;
            SetDrawViewmodel( client, ( g_fShowModel[client] != 1 ) ? false : true );
        }
    }
    else
    {
        g_fShowModel[client] = 1;
        SetDrawViewmodel( client, ( g_fShowModel[client] != 1 ) ? false : true );
    }    
}

public Action Cmd_Viewmodel( int client, int args )
{
    if ( !client ) return Plugin_Handled;

    if(g_fShowModel[client] != 1)
    {
        g_fShowModel[client] = 1;
    } 
    else
    {
        g_fShowModel[client] = 0;
    }    
    
    bool draw = ( g_fShowModel[client] != 1 ) ? false: true;
    
    SetDrawViewmodel( client, draw );
        
    return Plugin_Handled;
}

stock bool SetDrawViewmodel( int client, bool mode )
{
    SetEntProp( client, Prop_Data, "m_bDrawViewmodel", mode );
}