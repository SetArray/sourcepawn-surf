#include <sourcemod>
#include <clientprefs>
#include <sdktools>

#include <influx/core>

#undef REQUIRE_PLUGIN
#include <influx/hud>
#include <influx/options>

Handle g_hCookie_RecSounds;
int g_fRecSounds[MAXPLAYERS+1];

#define SOUNDS_FILE                 "pg_surfbhop_sounds.cfg"
#define PLATFORM_MAX_PATH_CELL      PLATFORM_MAX_PATH / 4

ArrayList g_hOtherSounds;
ArrayList g_hBestSounds;
ArrayList g_hPBSounds;
bool g_bLib_Hud;


public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Record Sounds",
    description = "",
    version = INF_VERSION
};

public void OnPluginStart()
{
    g_hOtherSounds = new ArrayList( PLATFORM_MAX_PATH_CELL );
    g_hBestSounds = new ArrayList( PLATFORM_MAX_PATH_CELL );
    g_hPBSounds = new ArrayList( PLATFORM_MAX_PATH_CELL );
    
    g_bLib_Hud = LibraryExists( INFLUX_LIB_HUD );

    if ( (g_hCookie_RecSounds = RegClientCookie( "influx_recsounds", INF_NAME..." Record Sounds", CookieAccess_Protected )) == null )
    {
        SetFailState( INF_CON_PRE..."Couldn't register recsounds cookie!" );
    }

    // CMDS
    RegConsoleCmd( "sm_recsounds", Cmd_RecSounds );    
}

public void Influx_OnRequestOptionsCmds()
{
    Influx_AddOptionsCommand( "recsounds", "Toggle game record sounds ON/OFF." );
}

public void OnLibraryAdded( const char[] lib )
{
    if ( StrEqual( lib, INFLUX_LIB_HUD ) ) g_bLib_Hud = true;
}

public void OnLibraryRemoved( const char[] lib )
{
    if ( StrEqual( lib, INFLUX_LIB_HUD ) ) g_bLib_Hud = false;
}

public void OnClientDisconnect(int client)
{
    if(IsFakeClient(client)) return;

    char szCookie[12];
    IntToString(g_fRecSounds[client], szCookie, sizeof(szCookie));
    SetClientCookie(client, g_hCookie_RecSounds, szCookie);    
}

public void OnClientCookiesCached(int client)
{
    if(AreClientCookiesCached(client))
    {
        char szCookie[12];
        GetClientCookie(client, g_hCookie_RecSounds, szCookie, sizeof(szCookie));

        if(szCookie[0] != '\0')
        {
            g_fRecSounds[client] = StringToInt(szCookie);
        }
        else
        {
            g_fRecSounds[client] = 1;
        }
    }
    else
    {
        g_fRecSounds[client] = 1;
    }    
}

public Action Cmd_RecSounds(int client, int args)
{
    if(!client) return Plugin_Handled;

    if(g_fRecSounds[client] != 1)
    {
        g_fRecSounds[client] = 1;
    } 
    else
    {
        g_fRecSounds[client] = 0;
    }

    return Plugin_Handled;    
}

public void Influx_OnRequestResultFlags()
{
    Influx_AddResultFlag( "Don't play record sound", RES_SND_DONTPLAY );
}

public void OnMapStart()
{
    ReadSounds();
}

stock bool ReadSounds()
{
    g_hOtherSounds.Clear();
    g_hBestSounds.Clear();
    g_hPBSounds.Clear();
    
    char szFile[PLATFORM_MAX_PATH];
    BuildPath( Path_SM, szFile, sizeof( szFile ), "configs/"...SOUNDS_FILE );
    
    KeyValues kv = new KeyValues( "Sounds" );
    kv.ImportFromFile( szFile );
    
    if ( !kv.GotoFirstSubKey() )
    {
        delete kv;
        return false;
    }
    
    char szPath[PLATFORM_MAX_PATH], szDownload[PLATFORM_MAX_PATH];
    
    do
    {
        if ( !kv.GetSectionName( szPath, sizeof( szPath ) ) )
        {
            continue;
        }
                
        int sounds_offset = 0;
        
        if (StrContains( szPath, "sound/", false ) == 0
        ||  StrContains( szPath, "sound\\", false ) == 0)
        {
            sounds_offset = 6;
            
            strcopy( szDownload, sizeof( szDownload ), szPath );
        }
        else
        {
            FormatEx( szDownload, sizeof( szDownload ), "sound/%s", szPath );
        }
        
        if ( !FileExists( szDownload, true ) )
        {
            LogError( INF_CON_PRE..."Sound file '%s' does not exist! Ignoring it.", szDownload );
            continue;
        }
        
        if ( PrecacheSound( szPath[sounds_offset] ) )
        {
            PrefetchSound( szPath[sounds_offset] );
            
            AddFileToDownloadsTable( szDownload );
        }
        else
        {
            LogError( INF_CON_PRE..."Couldn't precache record sound! Ignoring it. Sound: '%s'", szPath[sounds_offset] );
            
            continue;
        }
        
        if ( kv.GetNum( "pb", 0 ) )
        {
            g_hPBSounds.PushString( szPath[sounds_offset] );
        }
        
        if ( kv.GetNum( "best", 0 ) )
        {
            g_hBestSounds.PushString( szPath[sounds_offset] );
        }
        
    }
    while( kv.GotoNextKey() );
    
    delete kv;
    
    return true;
}

public void Influx_OnTimerFinishPost( int client, int runid, int mode, int style, float time, float prev_pb, float prev_best, int flags )
{
    // We don't want to play any sounds for this run!
    if ( flags & RES_SND_DONTPLAY ) return;
    
    decl String:szPath[PLATFORM_MAX_PATH];
    szPath[0] = '\0';
    
    if ( flags & (RES_TIME_FIRSTREC | RES_TIME_ISBEST) )
    {
        int len = g_hBestSounds.Length;
        
        if ( len )
        {
            g_hBestSounds.GetString( GetRandomInt( 0, len - 1 ), szPath, sizeof( szPath ) );
        }
    }
    else if ( flags & RES_TIME_PB )
    {
        int len = g_hPBSounds.Length;
        
        if ( len )
        {
            g_hPBSounds.GetString( GetRandomInt( 0, len - 1 ), szPath, sizeof( szPath ) );
        }
    }
    
    if ( szPath[0] != '\0' )
    {
        PlayRecordSound( szPath, client, time, prev_pb, prev_best );
    }
}

stock bool CanPlaySoundToClient( int client, int finisher, float time, float prev_pb, float prev_best )
{
    if(g_fRecSounds[client] != 1) return false;
    
    // Allow my own sounds.
    if ( finisher == client )
    {
        return true;
    }

    // Allow server record sounds.
    if( time < prev_best )
    {
        return true;
    }

    // Allow personal best sounds.
    if( time < prev_pb && time >= prev_best )
    {
        if(IsClientInGame(client) && IsClientObserver(client) && !IsFakeClient(client))
        {
            int sMode = GetEntProp(client, Prop_Send, "m_iObserverMode");
            if(sMode == 4 || sMode == 5)
            {
                int sTarg = GetEntPropEnt(client, Prop_Send, "m_hObserverTarget");
                if(sTarg == finisher)
                {
                    return true;
                }
            }
        }
    }
    
    return false;
}

stock void PlayRecordSound( const char[] szSound, int finisher, float time, float prev_pb, float prev_best )
{
    int[] clients = new int[MaxClients];
    int nClients = 0;
    
    for ( int i = 1; i <= MaxClients; i++ )
    {
        if ( IsClientInGame( i ) && !IsFakeClient( i ) )
        {
            if ( g_bLib_Hud )
            {
                if ( !CanPlaySoundToClient( i, finisher, time, prev_pb, prev_best ) )
                    continue;
            }
            
            clients[nClients++] = i;
        }
    }
    
    if ( nClients )
    {
        EmitSoundCompatible( clients, nClients, szSound );
    }
}

stock void EmitSoundCompatible( const int[] clients, int nClients, const char[] szSound )
{
    if ( GetEngineVersion() == Engine_CSGO )
    {
        decl String:szCommand[PLATFORM_MAX_PATH];
        FormatEx( szCommand, sizeof( szCommand ), "play */%s", szSound );
        
        for ( int i = 0; i < nClients; i++ )
        {
            ClientCommand( clients[i], szCommand );
        }
    }
    else
    {
        EmitSound( clients, nClients, szSound );
    }
}