#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <influx/core>

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Disable Ragdoll",
    description = "",
    version = INF_VERSION
};

public void OnPluginStart()
{
	HookEvent("player_death", OnPlayerDeath, EventHookMode_Pre);
}

public Action:OnPlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if(!client || !IsClientInGame(client))
		return Plugin_Continue;
	
	new entity = GetEntPropEnt(client, Prop_Send, "m_hRagdoll");
	if(entity > 0 && IsValidEdict(entity))
		AcceptEntityInput(entity, "Kill");
		
	return Plugin_Continue;
}