#include <sourcemod>

#include <influx/core>
#include <influx/options>
#include <influx/help>

#include <msharedutil/arrayvec>

#define MAX_CMD             32
#define MAX_CMD_CELL        MAX_CMD / 4

#define MAX_MSG             128
#define MAX_MSG_CELL        MAX_MSG / 4

enum
{
    C_CMD[MAX_CMD_CELL] = 0,
    C_MSG[MAX_MSG_CELL],

    C_SIZE
};

float g_flLastCmdO[INF_MAXPLAYERS];
ArrayList g_hComs;

// FORWARDS
Handle g_hForward_OnRequestOptionsCmds;

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Options Menu",
    description = "",
    version = INF_VERSION
};

public APLRes AskPluginLoad2( Handle hPlugin, bool late, char[] szError, int error_len )
{
    // LIBRARIES
    RegPluginLibrary( INFLUX_LIB_OPTIONS );
    
    // NATIVES
    CreateNative( "Influx_AddOptionsCommand", Native_AddOptionsCommand );
    CreateNative( "Influx_RemoveOptionsCommand", Native_RemoveOptionsCommand );
}

public void OnPluginStart()
{
    // FORWARDS
    g_hForward_OnRequestOptionsCmds = CreateGlobalForward( "Influx_OnRequestOptionsCmds", ET_Ignore );   
       
    // CMDS
    RegConsoleCmd( "sm_options", Cmd_Options );
    RegConsoleCmd( "sm_option", Cmd_Options );
    
    g_hComs = new ArrayList( C_SIZE );
}

public void OnAllPluginsLoaded()
{
    g_hComs.Clear();
    
    Call_StartForward( g_hForward_OnRequestOptionsCmds );
    Call_Finish();
}

public void OnClientPutInServer( int client )
{
    g_flLastCmdO[client] = 0.0;
}

public void Influx_RequestHelpCmds()
{
    Influx_AddHelpCommand( "options", "Opens the options menu." );
}

public Action Cmd_Options( int client, int args )
{
    decl data[C_SIZE];
    
    int len = GetArrayLength_Safe( g_hComs );
    int i;
    
    int num = 0;
    
    if ( IS_ENT_PLAYER( client ) )
    {
        if ( Inf_HandleCmdSpam( client, 3.0, g_flLastCmdO[client], true ) )
        {
            return Plugin_Handled;
        }
                
        decl String:szDisplay[128];
        
        Menu menu = new Menu( Hndlr_Empty );
        menu.SetTitle( "Player Options:\n " );
        
        for ( i = 0; i < len; i++ )
        {
            g_hComs.GetArray( i, data );
                        
            FormatEx( szDisplay, sizeof( szDisplay ), "%s - %s",
                data[C_CMD],
                data[C_MSG]);
            
            // ITEMDRAW_DEFAULT ITEMDRAW_DISABLED
            menu.AddItem( "", szDisplay, ITEMDRAW_DISABLED );
            
            ++num;
        }
        
        if ( !num )
        {
            menu.AddItem( "", "CMD_LOAD_ERROR", ITEMDRAW_DISABLED );
        }
        
        menu.Display( client, MENU_TIME_FOREVER );
    }
    
    return Plugin_Handled;
}

public int Hndlr_Empty( Menu menu, MenuAction action, int client, int index )
{
    MENU_HANDLE( menu, action )
    
    return 0;
}

// NATIVES
public int Native_AddOptionsCommand( Handle hPlugin, int nParms )
{
    if ( g_hComs == null ) return 0;
    
    int data[C_SIZE];
    
    GetNativeString( 1, view_as<char>( data[C_CMD] ), MAX_CMD );
    GetNativeString( 2, view_as<char>( data[C_MSG] ), MAX_MSG );
    
    g_hComs.PushArray( data );
    
    return 1;
}

public int Native_RemoveOptionsCommand( Handle hPlugin, int nParms )
{
    if ( g_hComs == null ) return 0;
    
    char szCmd[MAX_CMD], szCmd2[MAX_CMD];
    GetNativeString( 1, szCmd, sizeof( szCmd ) );
    
    int len = GetArrayLength_Safe( g_hComs );
    for ( int i = 0; i < len; i++ )
    {
        g_hComs.GetString( i, szCmd2, sizeof( szCmd2 ) );
        
        if ( StrEqual( szCmd, szCmd2, true ) )
        {
            g_hComs.Erase( i );
            return 1;
        }
    }
    
    return 0;
}