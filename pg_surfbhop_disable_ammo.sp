#include <sourcemod>
#include <cstrike>
#include <sdkhooks>
#include <smlib/weapons>

#include <influx/core>

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Disable Ammo",
    description = "",
    version = INF_VERSION
};

public OnPluginStart()
{	
	HookEventEx("weapon_fire", Event_WeaponFire, EventHookMode_Pre);
}

public OnClientPutInServer(client)
{
  if(!IsFakeClient(client)) 
  {
    SDKHook(client, SDKHook_WeaponEquipPost, OnWeapon);
    SDKHook(client, SDKHook_WeaponCanSwitchToPost, OnWeapon);
  }
}

public Action:Event_WeaponFire(Handle:event, const String:name[], bool:dontBroadcast)
{
	new id = GetEventInt(event,"userid"); 
	new client = GetClientOfUserId(id);
	OnWeapon(client);
	
	return Plugin_Handled;
}

public Action:OnWeapon(client)
{
	new Primary = GetPlayerWeaponSlot(client, CS_SLOT_PRIMARY);
	new Secondary = GetPlayerWeaponSlot(client, CS_SLOT_SECONDARY);

	if(Primary != -1)
	{
		Weapon_SetPrimaryClip(Primary, 0);
		ClearAmmo(client, Primary);
	}

	if (Secondary != -1)
	{
		Weapon_SetPrimaryClip(Secondary, 0);
		ClearAmmo(client, Secondary);
	}
}  

stock ClearAmmo(client, weapon)
{
	new ammotype = GetEntProp(weapon, Prop_Send, "m_iPrimaryAmmoType");
	if(ammotype == -1) return;

	SetEntProp(client, Prop_Send, "m_iAmmo", 0, _, ammotype);
	SetEntProp(weapon, Prop_Send, "m_iPrimaryReserveAmmoCount", 0);
	SetEntProp(weapon, Prop_Send, "m_iClip1", 0);
}