#include <sourcemod>
#include <cstrike>
#include <sdktools>

#include <influx/core>
#include <influx/teams>

#include <msharedutil/ents>

#undef REQUIRE_PLUGIN
#include <influx/pause>

int g_nSpawns_CT;
int g_nSpawns_T;

bool g_bLib_Pause;

public Plugin myinfo =
{
    author = INF_AUTHOR,
    url = INF_URL,
    name = INF_NAME..." - Team Manager",
    description = "",
    version = INF_VERSION
};

public APLRes AskPluginLoad2( Handle hPlugin, bool late, char[] szError, int error_len )
{
    // LIBRARIES
    RegPluginLibrary( INFLUX_LIB_TEAMS );
    
    // NATIVES
    CreateNative( "Influx_GetPreferredTeam", Native_GetPreferredTeam );
}

public void OnPluginStart()
{
    // CMDS
    RegConsoleCmd( "sm_spec", Cmd_Spec );
    RegConsoleCmd( "sm_spectate", Cmd_Spec );
    RegConsoleCmd( "sm_spectator", Cmd_Spec );
    
    RegConsoleCmd( "sm_spawn", Cmd_Spawn );
    RegConsoleCmd( "sm_respawn", Cmd_Spawn );
    
    AddCommandListener( Lstnr_Spawn, "sm_r" );
    AddCommandListener( Lstnr_Spawn, "sm_re" );
    AddCommandListener( Lstnr_Spawn, "sm_rs" );
    AddCommandListener( Lstnr_Spawn, "sm_restart" );
    AddCommandListener( Lstnr_Spawn, "sm_start" );
    
    AddCommandListener( Lstnr_JoinTeam, "jointeam" );
    
    g_bLib_Pause = LibraryExists( INFLUX_LIB_PAUSE );

    HookEvent( "player_spawn", E_PlayerSpawn );
    HookEvent( "player_death", E_PlayerDeath, EventHookMode_Pre );
}

public Action E_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
    return Plugin_Handled;
}

public void E_PlayerSpawn( Event event, const char[] szEvent, bool bImUselessWhyDoIExist )
{
    int client = GetClientOfUserId( event.GetInt( "userid" ) );
    if ( !client ) return;
    if ( !client || GetClientTeam( client ) <= CS_TEAM_SPECTATOR ) return;
    if ( !IsPlayerAlive( client ) ) return;
    
    FakeClientCommand(client, "sm_r");
}

public void OnLibraryAdded( const char[] lib )
{
    if ( StrEqual( lib, INFLUX_LIB_PAUSE ) ) g_bLib_Pause = true;
}

public void OnLibraryRemoved( const char[] lib )
{
    if ( StrEqual( lib, INFLUX_LIB_PAUSE ) ) g_bLib_Pause = false;
}

#if defined USE_LEVELINIT
public Action OnLevelInit( const char[] mapName, char mapEntities[2097152] )
#else
public void OnMapStart()
#endif
{
    GetSpawnCounts();
    
#if defined USE_LEVELINIT
    return Plugin_Continue;
#endif
}

public Action Cmd_Spec( int client, int args )
{
    if ( !client ) return Plugin_Handled;
    
    if ( g_bLib_Pause && IsPlayerAlive( client ) && Influx_GetClientState( client ) == STATE_RUNNING )
    {
        Influx_PauseClientRun( client );
    }
    
    ChangeClientTeam( client, CS_TEAM_SPECTATOR );
    
    if ( args )
    {
        // Attempt to find a name.
        char szArg[32];
        GetCmdArgString( szArg, sizeof( szArg ) );
        
        int targets[1];
        char szTemp[1];
        bool bUseless;
        if ( ProcessTargetString(
            szArg,
            0,
            targets,
            sizeof( targets ),
            COMMAND_FILTER_NO_MULTI,
            szTemp,
            sizeof( szTemp ),
            bUseless ) )
        {
            int target = targets[0];
            
            if (target != client
            &&  IS_ENT_PLAYER( target )
            &&  IsClientInGame( target )
            &&  IsPlayerAlive( target ) )
            {
                if ( GetClientObserverTarget( client ) != target )
                {
                    SetClientObserverTarget( client, target );
                    
                    Influx_PrintToChat( _, client, "You are now spectating {MAINCLR1}%N{CHATCLR}!", target );
                }
                
                SetClientObserverMode( client, OBS_MODE_IN_EYE );
            }
        }
    }
    
    return Plugin_Handled;
}

public Action Cmd_Spawn( int client, int args )
{
    if ( client )
    {
        SpawnPlayer( client );
    }
    
    return Plugin_Handled;
}

public Action Lstnr_Spawn( int client, const char[] command, int argc )
{
    if ( client && IsClientInGame( client ) )
    {
        SpawnPlayer( client );
    }
    
    return Plugin_Continue;
}

public Action Lstnr_JoinTeam( int client, const char[] command, int argc )
{
    if(client && argc)
    {
        char szTeam[2];
        GetCmdArg(1, szTeam, 2);
        switch(StringToInt(szTeam))
        {
            case CS_TEAM_T: {
                int team = GetClientTeam(client);
                if(team != CS_TEAM_T)
                {
                    ForcePlayerSuicide(client);
                    ChangeClientTeam(client, CS_TEAM_T);
                    CS_RespawnPlayer(client);
                }
            }
            case CS_TEAM_CT: {
                int team = GetClientTeam(client);
                if(team != CS_TEAM_CT)
                {
                    ForcePlayerSuicide(client);
                    ChangeClientTeam(client, CS_TEAM_CT);
                    CS_RespawnPlayer(client);
                }  
            }
        }
    }
    return Plugin_Continue;
}

stock void GetSpawnCounts()
{
    g_nSpawns_CT = 0;
    g_nSpawns_T = 0;
    
    int ent = -1;
    while ( (ent = FindEntityByClassname( ent, "info_player_counterterrorist" )) != -1 ) g_nSpawns_CT++;
    ent = -1;
    while ( (ent = FindEntityByClassname( ent, "info_player_terrorist" )) != -1 ) g_nSpawns_T++;
    
}

stock int GetPreferredTeam()
{
    if ( g_nSpawns_CT <= 0 && g_nSpawns_T <= 0 )
    {
        GetSpawnCounts();
    }
    
    if ( GetTeamClientCount( CS_TEAM_CT ) < g_nSpawns_CT )
    {
        return CS_TEAM_CT;
    }
    else if ( GetTeamClientCount( CS_TEAM_T ) < g_nSpawns_T )
    {
        return CS_TEAM_T;
    }
    else // Our spawns are full!
    {
        // Check if there are any bots to take over.
        for ( int i = 1; i <= MaxClients; i++ )
        {
            if ( IsClientInGame( i ) && GetClientTeam( i ) > CS_TEAM_SPECTATOR && IsFakeClient( i ) )
            {
                return GetClientTeam( i );
            }
        }
    }
    
    LogError( INF_CON_PRE..."Couldn't find optimal team to join! Assuming CT." );
    
    // Else, return default.
    return CS_TEAM_CT;
}

stock void SpawnPlayer( int client )
{
    if ( IsPlayerAlive( client ) ) return;
    if(GetClientTeam(client) != CS_TEAM_T && GetClientTeam(client) != CS_TEAM_CT)
    {
        ChangeClientTeam(client, CS_TEAM_CT);
    }
    
    if ( !IsPlayerAlive( client ) )
    {
        CS_RespawnPlayer( client );
    }
}

// NATIVES
public int Native_GetPreferredTeam( Handle hPlugin, int nParms )
{
    return GetPreferredTeam();
}